import React from "react";
import Spinner from "../common/Spinner";

const UndefinedActivity = () => {
  return <Spinner />;
};

export default UndefinedActivity;
