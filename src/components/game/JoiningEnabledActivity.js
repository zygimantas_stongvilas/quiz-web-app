import React from "react";
import PropTypes from "prop-types";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  CircularProgress,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    margin: theme.spacing(1),
    position: "relative",
    width: "100%",
  },
  progress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  media: {
    height: 250,
    // paddingTop: "56.25%", // 16:9
  },
}));

const JoiningEnabledActivity = ({
  session,
  participantName,
  onChange,
  onSubmit,
  hasJoined,
  joining = false,
  errors = {},
}) => {
  const classes = useStyles();

  const formId = "manage-quiz-form";

  return (
    <Card>
      {session.quiz.imageUrl && (
        <CardMedia
          className={classes.media}
          image={session.quiz.imageUrl}
          title="Quiz Banner"
        />
      )}
      <CardContent>
        <Typography
          align="center"
          variant="h5"
          color="textPrimary"
          gutterBottom
        >
          {session.quiz.title}
        </Typography>
        <Typography gutterBottom align="center" variant="body1">
          {session.code}
        </Typography>
        {hasJoined ? (
          <Typography align="center" variant="body1" color="textSecondary">
            You have joined as {participantName}. All you have to do now is wait
            for questions to show up!
          </Typography>
        ) : (
          <>
            <Typography
              align="center"
              variant="body1"
              color="textSecondary"
              gutterBottom
            >
              The session is starting! Enter your username to participate!
            </Typography>
            <form id={formId} onSubmit={onSubmit} autoComplete="off" noValidate>
              <TextField
                error={!!errors.name}
                helperText={errors.name}
                fullWidth
                required
                label="Username"
                value={participantName}
                onChange={onChange}
                disabled={joining}
              />
            </form>
          </>
        )}
      </CardContent>
      {!hasJoined && (
        <CardActions>
          <div className={classes.wrapper}>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              type="submit"
              form={formId}
              disabled={joining}
            >
              Join
            </Button>
            {joining && (
              <CircularProgress className={classes.progress} size={24} />
            )}
          </div>
        </CardActions>
      )}
    </Card>
  );
};

JoiningEnabledActivity.propTypes = {
  session: PropTypes.object.isRequired,
  participantName: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  hasJoined: PropTypes.bool.isRequired,
  joining: PropTypes.bool,
  errors: PropTypes.object,
};

export default JoiningEnabledActivity;
