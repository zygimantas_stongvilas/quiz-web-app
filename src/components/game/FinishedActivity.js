import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Avatar,
  Card,
  CardContent,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  parent: {
    "& > :not(:first-child)": {
      marginTop: 8,
    },
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  card: {
    // height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardContent: {
    flexGrow: 1,
  },
  currentPlayerAvatar: {
    color: theme.palette.getContrastText(theme.palette.primary.main),
    backgroundColor: theme.palette.primary.main,
  },
  gridContainer: {
    height: "100%",
  },
  scoresCard: {
    marginTop: 8,
  },
}));

const FinishedActivity = ({ leaderboards, playerId }) => {
  const classes = useStyles();

  const maxScore = leaderboards[0]?.score;
  const winners = leaderboards.filter(
    (participant) => participant.score === maxScore
  );

  return (
    <>
      <Card className={classes.card}>
        {leaderboards.length === 0 ? (
          <CardContent className={classes.cardContent}>
            <Grid
              className={classes.gridContainer}
              container
              alignItems="center"
            >
              <Grid item xs={12}>
                <Typography variant="h5" color="textPrimary" align="center">
                  Looks like no one participated...
                </Typography>
              </Grid>
            </Grid>
          </CardContent>
        ) : (
          <CardContent className={classes.cardContent}>
            <Typography
              variant="h5"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              Congratulations to the winner{winners.length > 1 ? "s" : ""}!
            </Typography>
            {winners.map((winner) => (
              <Typography
                key={winner.id}
                variant="h3"
                align="center"
                color="textPrimary"
                gutterBottom
              >
                {winner.name}
              </Typography>
            ))}
            <Typography
              variant="h5"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              Who won with a score of
            </Typography>
            <Typography
              variant="h3"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              {maxScore}
            </Typography>
          </CardContent>
        )}
      </Card>
      {leaderboards.length > 0 && (
        <Card className={clsx(classes.card, classes.scoresCard)}>
          <CardContent className={classes.cardContent}>
            <Typography variant="h5" color="textPrimary" gutterBottom>
              Scores
            </Typography>
            {leaderboards.length === 0 ? (
              <Typography>No one has joined yet!</Typography>
            ) : (
              <List>
                {leaderboards.map((participant, index) => (
                  <ListItem
                    key={participant.id}
                    divider={index < leaderboards.length - 1}
                  >
                    <ListItemAvatar>
                      <Avatar
                        className={clsx(
                          playerId &&
                            participant.id === playerId &&
                            classes.currentPlayerAvatar
                        )}
                      >
                        {index + 1}
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={participant.name} />
                    <ListItemSecondaryAction>
                      <Typography>{participant.score}</Typography>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
              </List>
            )}
          </CardContent>
        </Card>
      )}
    </>
  );
};

FinishedActivity.propTypes = {
  leaderboards: PropTypes.array.isRequired,
  playerId: PropTypes.number,
};

export default FinishedActivity;
