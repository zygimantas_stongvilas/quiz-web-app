import React from "react";
import PropTypes from "prop-types";
import { Card, CardContent, CardMedia, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Moment from "react-moment";

const useStyles = makeStyles(() => ({
  media: {
    height: 250,
    // paddingTop: "56.25%", // 16:9
  },
}));

const WaitingForActivationActivity = ({ session }) => {
  const classes = useStyles();

  return (
    <Card>
      {session.quiz.imageUrl && (
        <CardMedia
          className={classes.media}
          image={session.quiz.imageUrl}
          title="Quiz Banner"
        />
      )}
      <CardContent>
        <Typography
          align="center"
          variant="h5"
          color="textPrimary"
          gutterBottom
        >
          {session.quiz.title}
        </Typography>
        <Typography gutterBottom align="center" variant="body1">
          {session.code}
        </Typography>
        <Typography align="center" variant="body1" color="textSecondary">
          This session is scheduled to start at{" "}
          <Moment format="YYYY-MM-DD HH:mm" interval={0}>
            {new Date(session.scheduledTime)}
          </Moment>
        </Typography>
      </CardContent>
    </Card>
  );
};

WaitingForActivationActivity.propTypes = {
  session: PropTypes.object.isRequired,
};

export default WaitingForActivationActivity;
