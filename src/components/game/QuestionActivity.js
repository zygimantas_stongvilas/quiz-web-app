import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  LinearProgress,
  Typography,
} from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme, makeStyles } from "@material-ui/core/styles";
import { green, red } from "@material-ui/core/colors";

const regularTheme = createMuiTheme({});

const correctTheme = createMuiTheme({
  palette: {
    primary: green,
  },
});

const wrongTheme = createMuiTheme({
  palette: {
    primary: red,
  },
});

const useStyles = makeStyles((theme) => ({
  cardDisabled: {
    borderColor: theme.palette.error.main,
  },
  cardCorrect: {
    borderColor: theme.palette.success.main,
  },
  actions: {
    display: "block",
    "& > :not(:first-child)": {
      marginTop: 8,
    },
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    objectFit: "scale-down",
    backgroundSize: "contain",
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
}));

const normaliseProgress = (value, maxValue) =>
  maxValue ? (value * 100) / maxValue : 0;

const QuestionActivity = ({
  question,
  disabled,
  revealed,
  selectedAnswerId,
  onSelect,
  remainingTime,
}) => {
  const classes = useStyles();

  const disableInteraction = disabled || (question.time && remainingTime === 0);

  return (
    <Card
      className={clsx(
        (classes.card,
        revealed &&
          selectedAnswerId ===
            question.answers.find((answer) => answer.isCorrect).id &&
          classes.cardCorrect) ||
          (disableInteraction && classes.cardDisabled)
      )}
      variant="outlined"
    >
      {((revealed && question.revealedImageUrl) || question.imageUrl) && (
        <CardMedia
          className={classes.cardMedia}
          image={
            revealed && question.revealedImageUrl
              ? question.revealedImageUrl
              : question.imageUrl
          }
          title="Question Image"
        />
      )}
      <CardContent className={classes.cardContent}>
        <Typography align="center" variant="h5" color="textPrimary">
          {question.text}
        </Typography>
      </CardContent>
      <CardActions className={classes.actions} disableSpacing>
        <ThemeProvider theme={regularTheme}>
          {question.answers.map((answer) => {
            const button = (
              <Button
                variant={
                  answer.id === selectedAnswerId ? "contained" : "outlined"
                }
                color="primary"
                key={answer.id}
                disableFocusRipple={disableInteraction}
                disableRipple={disableInteraction}
                fullWidth
                onClick={() => onSelect(answer.id)}
              >
                {answer.text}
              </Button>
            );

            if (revealed) {
              if (answer.isCorrect) {
                return (
                  <ThemeProvider key={answer.id} theme={correctTheme}>
                    {button}
                  </ThemeProvider>
                );
              } else if (answer.id === selectedAnswerId) {
                return (
                  <ThemeProvider key={answer.id} theme={wrongTheme}>
                    {button}
                  </ThemeProvider>
                );
              }
            }
            return button;
          })}
        </ThemeProvider>
      </CardActions>
      {question.time && (
        <CardContent>
          <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
              <LinearProgress
                variant="determinate"
                value={normaliseProgress(remainingTime, question.time)}
              />
            </Box>
            <Box>
              <Typography variant="body2">
                {Math.ceil(remainingTime)}
              </Typography>
            </Box>
          </Box>
        </CardContent>
      )}
    </Card>
  );
};

QuestionActivity.propTypes = {
  question: PropTypes.object.isRequired,
  disabled: PropTypes.bool.isRequired,
  revealed: PropTypes.bool.isRequired,
  selectedAnswerId: PropTypes.number,
  onSelect: PropTypes.func.isRequired,
  remainingTime: PropTypes.number.isRequired,
};

export default QuestionActivity;
