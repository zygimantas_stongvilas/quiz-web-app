import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Avatar,
  Card,
  CardContent,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  parent: {
    "& > :not(:first-child)": {
      marginTop: 8,
    },
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardContent: {
    flexGrow: 1,
  },
  currentPlayerAvatar: {
    color: theme.palette.getContrastText(theme.palette.primary.main),
    backgroundColor: theme.palette.primary.main,
  },
}));

const ShowLeaderboardsActivity = ({ leaderboards, showPoints, playerId }) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
        <Typography variant="h5" color="textPrimary" gutterBottom>
          {showPoints
            ? "Scores"
            : `Participants${
                leaderboards.length > 0 ? ` (${leaderboards.length})` : ""
              }`}
        </Typography>
        {leaderboards.length === 0 ? (
          <Typography>No one has joined yet!</Typography>
        ) : (
          <List>
            {leaderboards.map((participant, index) => (
              <ListItem
                key={participant.id}
                divider={index < leaderboards.length - 1}
              >
                <ListItemAvatar>
                  <Avatar
                    className={clsx(
                      playerId &&
                        participant.id === playerId &&
                        classes.currentPlayerAvatar
                    )}
                  >
                    {index + 1}
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={participant.name} />
                {showPoints && (
                  <ListItemSecondaryAction>
                    <Typography>{participant.score}</Typography>
                  </ListItemSecondaryAction>
                )}
              </ListItem>
            ))}
          </List>
        )}
      </CardContent>
    </Card>
  );
};

ShowLeaderboardsActivity.propTypes = {
  leaderboards: PropTypes.array.isRequired,
  showPoints: PropTypes.bool,
  playerId: PropTypes.number,
};

export default ShowLeaderboardsActivity;
