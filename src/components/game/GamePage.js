import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useSnackbar } from "notistack";
import PropTypes from "prop-types";
import { HubConnectionBuilder } from "@microsoft/signalr";
import { Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import * as gameActions from "../../redux/actions/gameActions";
import activityType from "../../tools/activityType";
import CanceledActivity from "./CanceledActivity";
import FinishedActivity from "./FinishedActivity";
import JoiningEnabledActivity from "./JoiningEnabledActivity";
import Page from "../common/Page";
import QuestionActivity from "./QuestionActivity";
import ShowLeaderboardsActivity from "./ShowLeaderboardsActivity";
import Spinner from "../common/Spinner";
import UndefinedActivity from "./UndefinedActivity";
import WaitingForActivationActivity from "./WaitingForActivationActivity";
import { newSession, newParticipant } from "../../tools/mockData";
import { API_URL } from "../../api/apiUtils";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
  },
}));

const GamePage = ({
  sessionCode,
  session,
  question,
  leaderboards,
  loading,
  sessionStatusChanged,
  loadSession,
  joinGame,
  loadCurrentQuestion,
  submitAnswer,
  loadLeaderboards,
  ...props
}) => {
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();

  const [remainingTime, setRemainingTime] = useState(
    question?.remainingTime ? question.remainingTime : 0
  );
  const [timerReadyToStart, setTimerReadyToStart] = useState(false);

  const [participant, setParticipant] = useState(props.participant);

  const [selectedAnswerId, setSelectedAnswerId] = useState(null);

  const [joining, setJoining] = useState(false);
  const [errors, setErrors] = useState({});

  const [connection, setConnection] = useState(null);

  useEffect(() => {
    loadSession(sessionCode).catch((error) => {
      console.log(error);
      enqueueSnackbar("Failed to fetch session!", { variant: "error" });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!session.id) return;

    const newConnection = new HubConnectionBuilder()
      .withUrl(`${API_URL}/hubs/session?sessionId=${session.id}`)
      .withAutomaticReconnect()
      .build();

    setConnection(newConnection);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session.id]);

  useEffect(() => {
    if (connection) {
      connection
        .start()
        .then(() => connection.on("ReceiveStatus", handleServerSideEvent))
        .catch((e) => console.log("Connection failed: ", e));
      return () => connection.off("ReceiveStatus");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connection]);

  useEffect(() => {
    setParticipant({ ...props.participant });
  }, [props.participant]);

  useEffect(() => {
    if (!session.id) return;

    switch (session.activity) {
      case activityType.QUESTION_ENABLED:
        loadCurrentQuestion(session.id)
          .then(() => {
            setTimerReadyToStart(false);
            setSelectedAnswerId(null);
          })
          .catch((error) => {
            console.log(error);
            enqueueSnackbar("Failed to fetch current question!", {
              variant: "error",
            });
          });
        break;
      case activityType.QUESTION_DISABLED:
        if (session.currentQuestion === question.order) {
          if (selectedAnswerId)
            submitAnswer(session.id, {
              participantId: participant.id,
              questionId: question.id,
              answerId: selectedAnswerId,
            }).catch((error) => {
              console.log(error);
              enqueueSnackbar("Failed to submit answer!", {
                variant: "error",
              });
            });
        } else {
          loadCurrentQuestion(session.id).catch((error) => {
            console.log(error);
            enqueueSnackbar("Failed to fetch current question!", {
              variant: "error",
            });
          });
        }
        break;
      case activityType.QUESTION_REVEALED:
        if (session.currentQuestion !== question.order) {
          loadCurrentQuestion(session.id).catch((error) => {
            console.log(error);
            enqueueSnackbar("Failed to fetch current question!", {
              variant: "error",
            });
          });
        }
        break;
      case activityType.SHOW_LEADERBOARDS:
      case activityType.FINISHED:
        loadLeaderboards(session.id).catch((error) => {
          console.log(error);
          enqueueSnackbar("Failed to fetch leaderboard!", { variant: "error" });
        });
        break;
      default:
        break;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session.id, session.activity, session.currentQuestion]);

  useEffect(() => {
    if (!question?.time) return;
    if (!session.id) return;

    if (!timerReadyToStart) {
      setRemainingTime(question.remainingTime);
      setTimerReadyToStart(true);
    } else {
      if (
        session.activity === activityType.QUESTION_ENABLED &&
        question.remainingTime > 0
      ) {
        const timer = setInterval(() => {
          setRemainingTime((oldTime) => {
            if (oldTime <= 0) {
              clearInterval(timer);
              return 0;
            }
            return oldTime - 0.1;
          });
        }, 100);

        return () => clearInterval(timer);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session.id, question, session.activity, timerReadyToStart]);

  const handleServerSideEvent = (message) => {
    sessionStatusChanged(session.id, message);
  };

  const handleParticipantNameChange = (event) => {
    setParticipant((prevParticipant) => ({
      ...prevParticipant,
      name: event.target.value,
    }));
  };

  const handleJoinSubmit = (event) => {
    event.preventDefault();
    setErrors({});

    const isUsernameValid = () => {
      const errors = {};

      if (!participant.name) errors.name = "Username is required.";
      else if (!/^[a-zA-Z0-9]+$/.test(participant.name))
        errors.name = "Username can contain only letters and digits.";

      setErrors(errors);
      return Object.keys(errors).length === 0;
    };

    if (isUsernameValid()) {
      setJoining(true);
      joinGame(session.id, participant.name).catch((error) => {
        setJoining(false);
        setErrors({ name: JSON.parse(error.message).error });
        console.log(error);
        // enqueueSnackbar("Failed to join!", {
        //   variant: "error",
        // });
      });
    }
  };

  const handleSelectedAnswerChange = (answerId) => {
    if (session.activity !== activityType.QUESTION_ENABLED) return;
    setSelectedAnswerId(answerId);
  };

  return (
    <Page>
      <Container className={classes.container} maxWidth="md">
        {loading ? (
          <Spinner />
        ) : session.activity === activityType.UNDEFINED ? (
          <UndefinedActivity />
        ) : session.activity === activityType.WAITING_FOR_ACTIVATION ? (
          <WaitingForActivationActivity session={session} />
        ) : session.activity === activityType.JOINING_ENABLED ||
          !participant.id ? (
          <JoiningEnabledActivity
            session={session}
            participantName={participant.name || ""}
            onChange={handleParticipantNameChange}
            onSubmit={handleJoinSubmit}
            hasJoined={participant && session.id === participant.sessionId}
            joining={joining}
            errors={errors}
          />
        ) : (session.activity === activityType.QUESTION_ENABLED ||
            session.activity === activityType.QUESTION_DISABLED ||
            session.activity === activityType.QUESTION_REVEALED) &&
          question.id ? (
          <QuestionActivity
            question={question}
            disabled={session.activity !== activityType.QUESTION_ENABLED}
            revealed={session.activity === activityType.QUESTION_REVEALED}
            selectedAnswerId={selectedAnswerId}
            onSelect={handleSelectedAnswerChange}
            remainingTime={
              session.activity === activityType.QUESTION_ENABLED
                ? remainingTime
                : 0
            }
          />
        ) : session.activity === activityType.SHOW_LEADERBOARDS ? (
          <ShowLeaderboardsActivity
            leaderboards={leaderboards}
            playerId={participant.id}
            showPoints
          />
        ) : session.activity === activityType.FINISHED ? (
          // &&
          //   leaderboards &&
          //   leaderboards.length > 0
          <FinishedActivity
            leaderboards={leaderboards}
            playerId={participant.id}
            // winner={leaderboards[0]}
            // amIWinner={participant && leaderboards[0].id === participant.id}
          />
        ) : session.activity === activityType.CANCELED ? (
          <CanceledActivity />
        ) : (
          <UndefinedActivity />
        )}
      </Container>
    </Page>
  );
};

GamePage.propTypes = {
  session: PropTypes.object.isRequired,
  sessionCode: PropTypes.string.isRequired,
  participant: PropTypes.object.isRequired,
  question: PropTypes.object.isRequired,
  leaderboards: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  sessionStatusChanged: PropTypes.func.isRequired,
  loadSession: PropTypes.func.isRequired,
  joinGame: PropTypes.func.isRequired,
  loadCurrentQuestion: PropTypes.func.isRequired,
  submitAnswer: PropTypes.func.isRequired,
  loadLeaderboards: PropTypes.func.isRequired,
};

const mapStateToProps = ({ game, apiCallsInProgress }, ownProps) => {
  const sessionCode = ownProps.match.params.id;
  const session =
    sessionCode &&
    game.sessionDetails?.code &&
    game.sessionDetails.code.toLowerCase() === sessionCode.toLowerCase()
      ? game.sessionDetails
      : newSession;
  const participant =
    game.participant.sessionId === session.id
      ? game.participant
      : newParticipant;

  return {
    session,
    sessionCode,
    participant,
    question: game.question,
    leaderboards: game.leaderboards,
    loading:
      apiCallsInProgress.loadingGameSession ||
      apiCallsInProgress.loadingGameQuestion ||
      apiCallsInProgress.loadingGameLeaderboards,
  };
};

const mapDispatchToProps = {
  sessionStatusChanged: gameActions.sessionStatusChanged,
  loadSession: gameActions.loadSession,
  joinGame: gameActions.joinGame,
  loadCurrentQuestion: gameActions.loadCurrentQuestion,
  submitAnswer: gameActions.submitAnswer,
  loadLeaderboards: gameActions.loadLeaderboards,
};

export default connect(mapStateToProps, mapDispatchToProps)(GamePage);
