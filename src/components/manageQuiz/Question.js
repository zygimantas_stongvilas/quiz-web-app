import React from "react";
import PropTypes from "prop-types";
import {
  Button,
  Card,
  CardContent,
  CardMedia,
  Checkbox,
  FormControlLabel,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/core/styles";
import Answer from "./Answer";

const useStyles = makeStyles(() => ({
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    // paddingTop: "56.25%", // 16:9
    objectFit: "scale-down",
    backgroundSize: "contain",
    height: 250,
  },
  cardContent: {
    flexGrow: 1,
  },
  cardTitle: {
    marginBottom: 8.4,
  },
}));

const Question = ({
  question,
  order,
  onChange,
  onRemove,
  onAnswerChange,
  onAddNewAnswer,
  onRemoveAnswer,
  errors = {},
}) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <Grid container spacing={1}>
        {question.imageUrl && (
          <Grid item xs={12} sm={question.revealedImageUrl ? 6 : 12}>
            <CardMedia
              className={classes.cardMedia}
              image={question.imageUrl}
              title="Question Image"
            />
          </Grid>
        )}
        {question.revealedImageUrl && (
          <Grid item xs={12} sm={question.imageUrl ? 6 : 12}>
            <CardMedia
              className={classes.cardMedia}
              image={question.revealedImageUrl}
              title="Revealed Question Image"
            />
          </Grid>
        )}
      </Grid>
      <CardContent className={classes.cardContent}>
        <Grid className={classes.cardTitle} container alignItems="center">
          <Grid item xs={true}>
            <Typography variant="h5">Question #{order + 1}</Typography>
          </Grid>
          <Grid item>
            <IconButton onClick={() => onRemove(question.id)}>
              <DeleteIcon />
            </IconButton>
          </Grid>
        </Grid>
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={12}>
            <TextField
              error={!!errors.text}
              helperText={errors.text}
              name="text"
              label="Question"
              value={question.text || ""}
              onChange={(event) => onChange(event, question.id)}
              multiline
              required
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="imageUrl"
              label="Image"
              value={question.imageUrl || ""}
              onChange={(event) => onChange(event, question.id)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="revealedImageUrl"
              label="Revealed Image"
              value={question.revealedImageUrl || ""}
              onChange={(event) => onChange(event, question.id)}
              fullWidth
            />
          </Grid>
          <Grid item xs={6}>
            <FormControlLabel
              label="Timed"
              control={
                <Checkbox
                  color="primary"
                  name="isTimed"
                  checked={question.isTimed}
                  onChange={(event) => onChange(event, question.id)}
                />
              }
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              error={question.isTimed && !!errors.time}
              helperText={question.isTimed ? errors.time : ""}
              label="Time"
              type="number"
              name="time"
              value={question.isTimed ? question.time || "" : ""}
              disabled={!question.isTimed}
              required={question.isTimed}
              onChange={(event) => onChange(event, question.id)}
              fullWidth
              InputProps={{
                endAdornment: <InputAdornment position="end">s</InputAdornment>,
              }}
              inputProps={{ min: 1 }}
            />
          </Grid>
          <Grid item xs={12}>
            <FormControlLabel
              label="Show Leaderboard"
              control={
                <Checkbox
                  color="primary"
                  name="showLeaderboards"
                  checked={question.showLeaderboards}
                  onChange={(event) => onChange(event, question.id)}
                />
              }
            />
          </Grid>
          {question.answers.map((answer) => (
            <Grid key={answer.id} item xs={12}>
              <Answer
                answer={answer}
                onChange={(event) =>
                  onAnswerChange(event, question.id, answer.id)
                }
                onRemove={() => onRemoveAnswer(question.id, answer.id)}
                errors={errors.answers ? errors.answers[answer.id] : undefined}
              />
            </Grid>
          ))}
          {errors.correctAnswers && (
            <Grid item xs={12}>
              <Typography color="error" variant="body1">
                {errors.correctAnswers}
              </Typography>
            </Grid>
          )}
          <Grid item xs={12}>
            <Button
              variant="outlined"
              color="primary"
              fullWidth
              onClick={() => onAddNewAnswer(question.id)}
            >
              Add Answer
            </Button>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

const areEqual = (prevProps, nextProps) => {
  const { question: prevQuestion, errors: prevErrors } = prevProps;
  const { question: nextQuestion, errors: nextErrors } = nextProps;

  return (
    prevQuestion.text === nextQuestion.text &&
    prevQuestion.imageUrl === nextQuestion.imageUrl &&
    prevQuestion.revealedImageUrl === nextQuestion.revealedImageUrl &&
    prevQuestion.isTimed === nextQuestion.isTimed &&
    prevQuestion.time === nextQuestion.time &&
    prevQuestion.showLeaderboards === nextQuestion.showLeaderboards &&
    prevQuestion.answers === nextQuestion.answers &&
    prevErrors === nextErrors
  );
};

Question.propTypes = {
  question: PropTypes.object.isRequired,
  order: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  onAnswerChange: PropTypes.func.isRequired,
  onAddNewAnswer: PropTypes.func.isRequired,
  onRemoveAnswer: PropTypes.func.isRequired,
  errors: PropTypes.object,
};

export default React.memo(Question, areEqual);
