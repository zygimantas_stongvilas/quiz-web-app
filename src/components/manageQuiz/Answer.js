import React from "react";
import PropTypes from "prop-types";
import {
  FormControlLabel,
  Grid,
  IconButton,
  Radio,
  TextField,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

const Answer = ({ answer, onChange, onRemove, errors = {} }) => {
  return (
    <Grid container spacing={1}>
      <Grid item>
        <FormControlLabel
          label="Correct"
          labelPlacement="bottom"
          control={
            <Radio
              color="primary"
              checked={answer.isCorrect}
              name="isCorrect"
              onChange={onChange}
            />
          }
        />
      </Grid>
      <Grid item xs={true}>
        <TextField
          error={!!errors.text}
          helperText={errors.text}
          size="small"
          label="Answer"
          name="text"
          value={answer.text || ""}
          onChange={onChange}
          multiline
          fullWidth
          required
        />
      </Grid>
      <Grid item>
        <IconButton onClick={onRemove}>
          <DeleteIcon />
        </IconButton>
      </Grid>
    </Grid>
  );
};

const areEqual = (prevProps, nextProps) => {
  const { answer: prevAnswer, errors: prevErrors } = prevProps;
  const { answer: nextAnswer, errors: nextErrors } = nextProps;

  return (
    prevAnswer.text === nextAnswer.text &&
    prevAnswer.isCorrect === nextAnswer.isCorrect &&
    prevErrors === nextErrors
  );
};

Answer.propTypes = {
  answer: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  errors: PropTypes.object,
};

export default React.memo(Answer, areEqual);
