import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardContent,
  CardMedia,
  Grid,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    // paddingTop: "56.25%", // 16:9
    // objectFit: "scale-down",
    // backgroundSize: "contain",
    height: 250,
  },
  cardContent: {
    flexGrow: 1,
  },
}));

const QuizForm = ({ quiz, onChange, errors = {} }) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      {quiz.imageUrl && (
        <CardMedia
          className={classes.cardMedia}
          image={quiz.imageUrl}
          title="Quiz Banner"
        />
      )}
      <CardContent className={classes.cardContent}>
        <Typography variant="h5" gutterBottom>
          General Information
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              error={!!errors.title}
              helperText={errors.title}
              name="title"
              label="Title"
              value={quiz.title || ""}
              onChange={onChange}
              required
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="description"
              label="Description"
              value={quiz.description || ""}
              onChange={onChange}
              fullWidth
              multiline
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="imageUrl"
              label="Banner"
              type="url"
              value={quiz.imageUrl || ""}
              onChange={onChange}
              fullWidth
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

const areEqual = (prevProps, nextProps) => {
  const { quiz: prevQuiz, errors: prevErrors } = prevProps;
  const { quiz: nextQuiz, errors: nextErrors } = nextProps;

  return (
    prevQuiz.title === nextQuiz.title &&
    prevQuiz.description === nextQuiz.description &&
    prevQuiz.imageUrl === nextQuiz.imageUrl &&
    prevErrors === nextErrors
  );
};

QuizForm.propTypes = {
  quiz: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  errors: PropTypes.object,
};

export default React.memo(QuizForm, areEqual);
