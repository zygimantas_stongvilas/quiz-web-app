import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { useSnackbar } from "notistack";
import PropTypes from "prop-types";
import {
  Button,
  CircularProgress,
  Container,
  Grid,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import * as quizActions from "../../redux/actions/quizActions";
import * as sessionActions from "../../redux/actions/sessionActions";
import Page from "../common/Page";
import Question from "./Question";
import QuizForm from "./QuizForm";
import Spinner from "../common/Spinner";
import { newQuiz, newQuestion, newAnswer } from "../../tools/mockData";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    margin: theme.spacing(1),
    position: "relative",
  },
  progress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  title: {
    marginBottom: 8.4,
  },
  container: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
  },
}));

const generateTemporaryId = (existingElements) => {
  if (!existingElements || existingElements.length === 0) return -1;

  const minId = existingElements
    .map((element) => element.id)
    .reduce((a, b) => Math.min(a, b));
  return minId >= 0 ? -1 : minId - 1;
};

const ManageQuizPage = ({
  isLoggedIn,
  quizId,
  loading,
  loadQuiz,
  saveQuiz,
  saveSession,
  ...props
}) => {
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();

  const [quiz, setQuiz] = useState(props.quiz);

  const [errors, setErrors] = useState({ general: {}, questions: {} });
  const [saving, setSaving] = useState(false);

  const [quizSaved, setQuizSaved] = useState(false);

  useEffect(() => {
    if (!isLoggedIn) return;
    if (quizId)
      loadQuiz(quizId).catch((error) => {
        console.log(error);
        enqueueSnackbar("Failed to fetch quiz!", {
          variant: "error",
        });
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!isLoggedIn) return;
    if (quizId && props.quiz.id === quizId) setQuiz({ ...props.quiz });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.quiz]);

  const handleSubmit = (event) => {
    event.preventDefault();
    setErrors({ general: {}, questions: {} });

    const quizFormIsValid = () => {
      const generalErrors = {};
      const questionsErrors = {};

      if (!quiz.title) generalErrors.title = "Title is required.";
      quiz.questions.forEach((question) => {
        if (!question.text)
          questionsErrors[question.id] = {
            ...questionsErrors[question.id],
            text: "Text is required.",
          };
        if (question.isTimed) {
          if (!question.time) {
            questionsErrors[question.id] = {
              ...questionsErrors[question.id],
              time: "Time is required.",
            };
          } else if (question.time < 1) {
            questionsErrors[question.id] = {
              ...questionsErrors[question.id],
              time: "Time must be an integer greater than 0.",
            };
          }
        }
        if (
          question.answers.filter((answer) => answer.isCorrect).length !== 1
        ) {
          questionsErrors[question.id] = {
            ...questionsErrors[question.id],
            correctAnswers: "Question must have 1 correct answer.",
          };
        }

        const answersErrors = {};
        question.answers.forEach((answer) => {
          if (!answer.text) {
            answersErrors[answer.id] = {
              ...answersErrors[answer.id],
              text: "Text is required.",
            };
          }
        });
        if (Object.keys(answersErrors).length > 0) {
          questionsErrors[question.id] = {
            ...questionsErrors[question.id],
            answers: answersErrors,
          };
        }
      });

      setErrors({ general: generalErrors, questions: questionsErrors });
      return (
        Object.keys(generalErrors).length === 0 &&
        Object.keys(questionsErrors).length === 0
      );
    };

    if (quizFormIsValid()) {
      setSaving(true);
      saveQuiz({
        ...quiz,
        questions: quiz.questions.map((question, questionIndex) => ({
          ...question,
          id: question.id > 0 ? question.id : null,
          order: questionIndex,
          answers: question.answers.map((answer, answerIndex) => ({
            ...answer,
            id: answer.id > 0 ? answer.id : null,
            order: answerIndex,
          })),
        })),
      })
        .then(() => {
          enqueueSnackbar(`Quiz ${quiz.id ? "updated" : "created"}!`, {
            variant: "success",
          });
          setQuizSaved(true);
          // setSaving(false);
        })
        .catch((error) => {
          console.log(error);
          enqueueSnackbar("Failed to save quiz!", {
            variant: "error",
          });
          setSaving(false);
        });
    }
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setQuiz((prevQuiz) => ({
      ...prevQuiz,
      [name]: value,
    }));
  };

  const handleQuestionChange = (event, questionId) => {
    const { name, value, checked } = event.target;

    setQuiz((prevQuiz) => ({
      ...prevQuiz,
      questions: prevQuiz.questions.map((question) =>
        question.id === questionId
          ? {
              ...question,
              [name]:
                name === "isTimed" || name === "showLeaderboards"
                  ? checked
                  : name === "time"
                  ? parseInt(value)
                  : value,
            }
          : question
      ),
    }));
  };

  const handleAnswerChange = (event, questionId, answerId) => {
    const { name, value } = event.target;

    if (name === "isCorrect") {
      setQuiz((prevQuiz) => ({
        ...prevQuiz,
        questions: prevQuiz.questions.map((question) =>
          question.id === questionId
            ? {
                ...question,
                answers: question.answers.map((answer) => ({
                  ...answer,
                  isCorrect: answerId === answer.id,
                })),
              }
            : question
        ),
      }));
    } else {
      setQuiz((prevQuiz) => ({
        ...prevQuiz,
        questions: prevQuiz.questions.map((question) =>
          question.id === questionId
            ? {
                ...question,
                answers: question.answers.map((answer) => {
                  if (answerId === answer.id) {
                    return {
                      ...answer,
                      [name]: value,
                    };
                  }
                  return answer;
                }),
              }
            : question
        ),
      }));
    }
  };

  const handleAddNewQuestion = () => {
    setQuiz((prevQuiz) => ({
      ...prevQuiz,
      questions: [
        ...prevQuiz.questions,
        { ...newQuestion, id: generateTemporaryId(prevQuiz.questions) },
      ],
    }));
  };

  const handleRemoveQuestion = (questionId) => {
    setQuiz((prevQuiz) => ({
      ...prevQuiz,
      questions: prevQuiz.questions.filter(
        (question) => question.id !== questionId
      ),
    }));
  };

  const handleAddNewAnswer = (questionId) => {
    setQuiz((prevQuiz) => ({
      ...prevQuiz,
      questions: prevQuiz.questions.map((question) =>
        question.id === questionId
          ? {
              ...question,
              answers: [
                ...question.answers,
                { ...newAnswer, id: generateTemporaryId(question.answers) },
              ],
            }
          : question
      ),
    }));
  };

  const handleRemoveAnswer = (questionId, answerId) => {
    setQuiz((prevQuiz) => ({
      ...prevQuiz,
      questions: prevQuiz.questions.map((question) =>
        question.id === questionId
          ? {
              ...question,
              answers: question.answers.filter(
                (answer) => answer.id !== answerId
              ),
            }
          : question
      ),
    }));
  };

  const formHasErrors = () => {
    return (
      Object.keys(errors.general).length > 0 ||
      Object.keys(errors.questions).length > 0
    );
  };

  const formId = "manage-quiz-form";

  return !isLoggedIn ? (
    <Redirect to="/" />
  ) : (
    <>
      {quizSaved && <Redirect push to="/quizzes" />}
      <Page>
        <Container maxWidth="lg" className={classes.container}>
          <Grid
            className={classes.title}
            container
            spacing={1}
            alignItems="center"
          >
            <Grid item xs={true}>
              <Typography variant="h4">
                {quizId ? "Edit Quiz" : "Create Quiz"}
              </Typography>
            </Grid>
            <Grid item>
              <div className={classes.wrapper}>
                <Button
                  variant="contained"
                  color={formHasErrors() ? "secondary" : "primary"}
                  type="submit"
                  form={formId}
                  disabled={saving}
                >
                  Save
                </Button>
                {saving && (
                  <CircularProgress className={classes.progress} size={24} />
                )}
              </div>
            </Grid>
          </Grid>
          {loading ? (
            <Spinner />
          ) : (
            <form
              id={formId}
              onSubmit={handleSubmit}
              autoComplete="off"
              noValidate
            >
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <QuizForm
                    quiz={quiz}
                    onChange={handleChange}
                    errors={errors.general}
                  />
                </Grid>
                {quiz.questions.map((question, index) => (
                  <Grid item xs={12} key={question.id}>
                    <Question
                      question={question}
                      order={index}
                      onChange={handleQuestionChange}
                      onRemove={handleRemoveQuestion}
                      onAnswerChange={handleAnswerChange}
                      onAddNewAnswer={handleAddNewAnswer}
                      onRemoveAnswer={handleRemoveAnswer}
                      errors={errors.questions[question.id]}
                    />
                  </Grid>
                ))}
                <Grid item xs={12}>
                  <Button
                    variant="outlined"
                    color="primary"
                    fullWidth
                    onClick={handleAddNewQuestion}
                  >
                    Add Question
                  </Button>
                </Grid>
              </Grid>
            </form>
          )}
        </Container>
      </Page>
    </>
  );
};

ManageQuizPage.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  quiz: PropTypes.object.isRequired,
  quizId: PropTypes.number,
  loading: PropTypes.bool.isRequired,
  loadQuiz: PropTypes.func.isRequired,
  saveQuiz: PropTypes.func.isRequired,
  saveSession: PropTypes.func.isRequired,
};

const mapStateToProps = ({ auth, quizzes, apiCallsInProgress }, ownProps) => {
  const quizId = ownProps.match.params.id
    ? parseInt(ownProps.match.params.id)
    : null;

  const quiz =
    quizId &&
    quizzes.activeQuizDetails &&
    quizzes.activeQuizDetails.id === quizId
      ? quizzes.activeQuizDetails
      : newQuiz;

  return {
    isLoggedIn: auth.isLoggedIn,
    quiz,
    quizId,
    loading: apiCallsInProgress.loadingQuiz > 0,
  };
};

const mapDispatchToProps = {
  loadQuiz: quizActions.loadQuiz,
  saveQuiz: quizActions.saveQuiz,
  saveSession: sessionActions.saveSession,
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageQuizPage);
