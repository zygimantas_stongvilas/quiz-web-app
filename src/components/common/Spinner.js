import React from "react";
import { CircularProgress, Grid } from "@material-ui/core";

const Spinner = () => (
  <Grid container justify="center">
    <Grid item>
      <CircularProgress size={150} />
    </Grid>
  </Grid>
);

export default Spinner;
