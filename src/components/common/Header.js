import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { useSnackbar } from "notistack";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  AppBar,
  Button,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  SwipeableDrawer,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AccountCircle from "@material-ui/icons/AccountCircle";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import HomeIcon from "@material-ui/icons/Home";
import LiveHelpIcon from "@material-ui/icons/LiveHelp";
import MenuIcon from "@material-ui/icons/Menu";
import GoogleLogin from "react-google-login";
import * as authActions from "../../redux/actions/authActions";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  toolbar: {
    paddingRight: 24,
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
}));

const Header = ({
  isLoggedIn,
  loggedInAs,
  loading,
  login,
  logout,
  loadUser,
}) => {
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();

  const history = useHistory();

  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  useEffect(() => {
    if (isLoggedIn && !loggedInAs) {
      loadUser().catch((error) => {
        console.log(error);
        enqueueSnackbar("Failed to fetch user data!", { variant: "error" });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoggedIn, loggedInAs]);

  const open = Boolean(anchorEl);

  const handleDrawerOpen = () => setIsDrawerOpen(true);
  const handleDrawerClose = () => setIsDrawerOpen(false);

  const handleMenuOpen = (event) => setAnchorEl(event.currentTarget);
  const handleMenuClose = () => setAnchorEl(null);

  const handleGoogleLoginSuccess = (response) => {
    login(response).catch((error) => {
      console.log(error);
      enqueueSnackbar("Failed to login!", { variant: "error" });
    });
  };

  const handleGoogleLoginFailure = (response) => {
    console.log(response);
  };

  const handleLogout = () => {
    handleMenuClose();
    logout().catch((error) => {
      console.log(error);
      enqueueSnackbar("Failed to logout!", { variant: "error" });
    });
  };

  const handleDrawerItemClick = (pathToPush) => {
    setIsDrawerOpen(false);
    history.push(pathToPush);
  };

  return (
    <>
      <AppBar position="absolute" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleDrawerOpen}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap className={classes.title}>
            QVizma
          </Typography>
          {isLoggedIn ? (
            <div>
              <IconButton
                onClick={handleMenuOpen}
                color="inherit"
                disabled={loading}
              >
                <AccountCircle />
              </IconButton>
              <Menu
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={!loading && open}
                onClose={handleMenuClose}
              >
                <MenuItem disabled>{loggedInAs}</MenuItem>
                <Divider />
                <MenuItem onClick={handleLogout}>Logout</MenuItem>
              </Menu>
            </div>
          ) : (
            <GoogleLogin
              disabled={loading}
              clientId={process.env.REACT_APP_AUTH_CLIENT_ID}
              render={(renderProps) => (
                <Button
                  onClick={renderProps.onClick}
                  disabled={renderProps.disabled}
                  color="inherit"
                >
                  Login
                </Button>
              )}
              onSuccess={handleGoogleLoginSuccess}
              onFailure={handleGoogleLoginFailure}
              cookiePolicy={"single_host_origin"}
            />
          )}
        </Toolbar>
      </AppBar>
      <SwipeableDrawer
        variant="temporary"
        onOpen={handleDrawerOpen}
        onClose={handleDrawerClose}
        classes={{
          paper: clsx(
            classes.drawerPaper,
            !isDrawerOpen && classes.drawerPaperClose
          ),
        }}
        open={isDrawerOpen}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          <ListItem button onClick={() => handleDrawerItemClick("/")}>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary="Home" />
          </ListItem>
          {isLoggedIn && (
            <ListItem button onClick={() => handleDrawerItemClick("/quizzes")}>
              <ListItemIcon>
                <LiveHelpIcon />
              </ListItemIcon>
              <ListItemText primary="Quizzes" />
            </ListItem>
          )}
        </List>
      </SwipeableDrawer>
    </>
  );
};

Header.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  loggedInAs: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  loadUser: PropTypes.func.isRequired,
};

const mapStateToProps = ({ auth, apiCallsInProgress }) => ({
  isLoggedIn: auth.isLoggedIn,
  loggedInAs: auth.isLoggedIn ? auth.userData.displayName : null,
  loading: auth.isLoggedIn && apiCallsInProgress.loadingUser,
});

const mapDispatchToProps = {
  login: authActions.login,
  logout: authActions.logout,
  loadUser: authActions.loadUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
