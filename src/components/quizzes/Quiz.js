import React, { useState } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Collapse,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  ListSubheader,
  Menu,
  MenuItem,
  Typography,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import EventIcon from "@material-ui/icons/Event";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import LockIcon from "@material-ui/icons/Lock";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { makeStyles } from "@material-ui/core/styles";
import Moment from "react-moment";
import activityType from "../../tools/activityType";

const useStyles = makeStyles((theme) => ({
  card: {
    // height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
}));

const getSessionStatusText = (session, totalQuestions) => {
  if (session.activity === activityType.WAITING_FOR_ACTIVATION)
    return "Scheduled";
  if (session.activity === activityType.FINISHED) return "Finished";
  if (
    session.activity !== activityType.UNDEFINED &&
    session.activity !== activityType.CANCELED
  )
    return `In Progress (question ${
      session.currentQuestion + 1
    }/${totalQuestions})`;
};

const Quiz = ({
  quiz,
  onScheduleSessionClick,
  onEditClick,
  onDeleteClick,
  onEditSessionClick,
  onDeleteSessionClick,
  onSessionClick,
}) => {
  const classes = useStyles();

  const [sessionsExpanded, setSessionsExpanded] = useState(false);
  const [sessionAnchorEl, setSessionAnchorEl] = useState(null);

  const [session, setSession] = useState({});

  const handleExpandSessionsClick = () =>
    setSessionsExpanded(!sessionsExpanded);

  const handleSessionMenuClick = (event, session) => {
    setSession(session);
    setSessionAnchorEl(event.currentTarget);
  };
  const handleSessionMenuClose = () => setSessionAnchorEl(null);

  const handleEditSessionClick = () => {
    handleSessionMenuClose();
    onEditSessionClick(session);
  };

  const handleDeleteSessionClick = () => {
    handleSessionMenuClose();
    onDeleteSessionClick(session);
  };

  const handleViewSessionClick = () => {
    handleSessionMenuClose();
    onSessionClick(session.code);
  };

  const handleSessionClick = (sessionCode) => {
    handleSessionMenuClose();
    onSessionClick(sessionCode);
  };

  return (
    <Grid key={quiz.id} item xs={12} sm={6} md={4}>
      <Card className={classes.card}>
        {quiz.imageUrl && (
          <CardMedia
            className={classes.cardMedia}
            image={quiz.imageUrl}
            title="Quiz Banner"
          />
        )}
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">
            {quiz.title}
          </Typography>
          <Typography>{quiz.description}</Typography>
          <Typography variant="body2" color="textSecondary">
            {quiz.questionsCount} question{quiz.questionsCount === 1 ? "" : "s"}
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton onClick={() => onScheduleSessionClick(quiz.id)}>
            <EventIcon />
          </IconButton>
          <IconButton onClick={() => onEditClick(quiz.id)}>
            <EditIcon />
          </IconButton>
          <IconButton onClick={() => onDeleteClick(quiz)}>
            <DeleteIcon />
          </IconButton>
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: sessionsExpanded,
            })}
            onClick={handleExpandSessionsClick}
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={sessionsExpanded} timeout="auto" unmountOnExit>
          <CardContent>
            <List
              subheader={<ListSubheader>Sessions</ListSubheader>}
              disablePadding
            >
              {quiz.sessions
                .sort(
                  (a, b) =>
                    new Date(a.scheduledTime) - new Date(b.scheduledTime)
                )
                .map((session) => (
                  <ListItem
                    key={session.id}
                    button
                    onClick={() => handleSessionClick(session.code)}
                  >
                    {session.isPrivate && (
                      <ListItemIcon>
                        <LockIcon />
                      </ListItemIcon>
                    )}
                    <ListItemText
                      primary={session.code}
                      secondary={
                        <>
                          <Moment format="YYYY-MM-DD HH:mm" interval={0}>
                            {new Date(session.scheduledTime)}
                          </Moment>
                          <br />
                          {getSessionStatusText(session, quiz.questionsCount)}
                        </>
                      }
                    />
                    <ListItemSecondaryAction>
                      <IconButton
                        edge="end"
                        onClick={(event) =>
                          handleSessionMenuClick(event, session)
                        }
                      >
                        <MoreVertIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
              <Menu
                anchorEl={sessionAnchorEl}
                keepMounted
                open={Boolean(sessionAnchorEl)}
                onClose={handleSessionMenuClose}
              >
                <MenuItem onClick={handleViewSessionClick}>View</MenuItem>
                <MenuItem onClick={handleEditSessionClick}>Edit</MenuItem>
                <MenuItem onClick={handleDeleteSessionClick}>Delete</MenuItem>
              </Menu>
            </List>
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  );
};

Quiz.propTypes = {
  quiz: PropTypes.object.isRequired,
  onScheduleSessionClick: PropTypes.func.isRequired,
  onEditClick: PropTypes.func.isRequired,
  onDeleteClick: PropTypes.func.isRequired,
  onEditSessionClick: PropTypes.func.isRequired,
  onDeleteSessionClick: PropTypes.func.isRequired,
  onSessionClick: PropTypes.func.isRequired,
};

export default Quiz;
