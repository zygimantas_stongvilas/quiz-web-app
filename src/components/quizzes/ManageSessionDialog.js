import "date-fns";
import React from "react";
import PropTypes from "prop-types";
import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  CircularProgress,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Grid,
  TextField,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardTimePicker,
} from "@material-ui/pickers";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    margin: theme.spacing(1),
    position: "relative",
  },
  progress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
}));

const ManageSessionDialog = ({
  session,
  isOpen,
  onChange,
  onSubmit,
  onCancel,
  saving = false,
  errors = {},
}) => {
  const classes = useStyles();

  const formId = "manage-quiz-form";

  return (
    <Dialog open={isOpen} onClose={onCancel}>
      <DialogTitle>{session.id ? "Edit" : "Schedule"} Session</DialogTitle>
      <DialogContent>
        <form id={formId} onSubmit={onSubmit} autoComplete="off" noValidate>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <KeyboardDatePicker
                  error={!!errors.scheduledTime}
                  helperText={errors.scheduledTime}
                  fullWidth
                  disableToolbar
                  autoComplete="off"
                  required
                  disabled={saving}
                  format="yyyy-MM-dd"
                  variant="inline"
                  margin="normal"
                  label="Date"
                  name="scheduledTime"
                  value={session.scheduledTime}
                  onChange={onChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <KeyboardTimePicker
                  error={!!errors.scheduledTime}
                  helperText={errors.scheduledTime}
                  fullWidth
                  ampm={false}
                  disableToolbar
                  keyboardIcon={<AccessTimeIcon />}
                  autoComplete="off"
                  required
                  disabled={saving}
                  variant="inline"
                  margin="normal"
                  label="Time"
                  name="scheduledTime"
                  value={session.scheduledTime}
                  onChange={onChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  error={!!errors.code}
                  helperText={errors.code}
                  name="code"
                  label="Code"
                  value={session.code || ""}
                  onChange={onChange}
                  disabled={saving}
                  required
                  fullWidth
                  inputProps={{ pattern: "[a-zA-z0-9]" }}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  label="Private"
                  control={
                    <Checkbox
                      color="primary"
                      name="isPrivate"
                      disabled={saving}
                      checked={session.isPrivate}
                      onChange={onChange}
                    />
                  }
                />
              </Grid>
            </Grid>
          </MuiPickersUtilsProvider>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel} color="primary" disabled={saving}>
          Cancel
        </Button>
        <div className={classes.wrapper}>
          <Button color="primary" disabled={saving} type="submit" form={formId}>
            Confirm
          </Button>
          {saving && (
            <CircularProgress className={classes.progress} size={24} />
          )}
        </div>
      </DialogActions>
    </Dialog>
  );
};

ManageSessionDialog.propTypes = {
  session: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object,
};

export default ManageSessionDialog;
