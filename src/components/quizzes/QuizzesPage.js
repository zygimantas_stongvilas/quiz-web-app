import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { useSnackbar } from "notistack";
import PropTypes from "prop-types";
import { Button, Container, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import * as quizActions from "../../redux/actions/quizActions";
import * as sessionActions from "../../redux/actions/sessionActions";
import AlertDialog from "../common/AlertDialog";
import ManageSessionDialog from "./ManageSessionDialog";
import Page from "../common/Page";
import Quiz from "./Quiz";
import Spinner from "../common/Spinner";
import { newSession } from "../../tools/mockData";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
  },
}));

const QuizzesPage = ({
  isLoggedIn,
  quizzes,
  loading,
  loadQuizzes,
  deleteQuiz,
  saveSession,
  deleteSession,
}) => {
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();

  const [redirectToCreateQuiz, setRedirectToCreateQuiz] = useState(false);
  const [redirectToQuizId, setRedirectToQuizId] = useState(null);
  const [redirectToSessionCode, setRedirectToSessionCode] = useState(null);

  const [session, setSession] = useState(newSession);
  const [isSessionDialogOpen, setIsSessionDialogOpen] = useState(false);

  const [sessionErrors, setSessionErrors] = useState({});
  const [savingSession, setSavingSession] = useState(false);

  const [quizToDelete, setQuizToDelete] = useState(null);
  const [quizAlertOpen, setQuizAlertOpen] = useState(false);

  const [sessionToDelete, setSessionToDelete] = useState(null);
  const [sessionAlertOpen, setSessionAlertOpen] = useState(false);

  useEffect(() => {
    if (!isLoggedIn) return;
    loadQuizzes().catch((error) => {
      console.log(error);
      enqueueSnackbar("Failed to fetch quizzes!", { variant: "error" });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleCreateQuizClick = () => {
    setRedirectToCreateQuiz(true);
  };

  const handleEditQuizClick = (quizId) => {
    setRedirectToQuizId(quizId);
  };

  const handleDeleteQuizClick = (quiz) => {
    setQuizToDelete(quiz);
    setQuizAlertOpen(true);
  };

  const handleDeleteQuizConfirm = () => {
    setQuizAlertOpen(false);
    deleteQuiz(quizToDelete.id)
      .then(() => {
        enqueueSnackbar("Quiz deleted!", { variant: "success" });
      })
      .catch((error) => {
        console.log(error);
        enqueueSnackbar("Failed to delete quiz!", { variant: "error" });
      });
  };

  const handleDeleteQuizCancel = () => {
    setQuizAlertOpen(false);
  };

  const handleScheduleSessionClick = (quizId) => {
    setSessionErrors({});
    setSession({ ...newSession, quizId, scheduledTime: new Date() });
    setIsSessionDialogOpen(true);
  };

  const handleSessionChange = (input) => {
    if (!input || input instanceof Date) {
      setSession((prevSession) => ({
        ...prevSession,
        scheduledTime: input,
      }));
    } else {
      const target = input.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      setSession((prevSession) => ({
        ...prevSession,
        [name]: value,
      }));
    }
  };

  const handleSessionSubmit = (event) => {
    event.preventDefault();
    setSessionErrors({});

    const sessionFormIsValid = () => {
      const errors = {};

      if (
        !(session.scheduledTime instanceof Date) ||
        isNaN(session.scheduledTime.getTime())
      ) {
        errors.scheduledTime = "Invalid date or time.";
      }

      if (!session.code) errors.code = "Code is required.";
      else if (!/^[a-zA-Z0-9]+$/.test(session.code))
        errors.code = "Code can contain only letters and digits.";

      setSessionErrors(errors);
      return Object.keys(errors).length === 0;
    };

    if (sessionFormIsValid()) {
      setSavingSession(true);
      saveSession(session)
        .then(() => {
          setIsSessionDialogOpen(false);
          setSavingSession(false);
          enqueueSnackbar(`Session ${session.id ? "updated" : "scheduled"}!`, {
            variant: "success",
          });
        })
        .catch((error) => {
          console.log(error);
          setSavingSession(false);
          setSessionErrors({ code: JSON.parse(error.message).error });
          // enqueueSnackbar(
          //   `Failed to ${session.id ? "update" : "schedule"} session!`,
          //   {
          //     variant: "error",
          //   }
          // );
        });
    }
  };

  const handleSessionCancel = () => {
    setIsSessionDialogOpen(false);
  };

  const handleEditSessionClick = (session) => {
    setSessionErrors({});
    setSession({
      ...session,
      scheduledTime: new Date(session.scheduledTime),
    });
    setIsSessionDialogOpen(true);
  };

  const handleDeleteSessionClick = (session) => {
    setSessionToDelete(session);
    setSessionAlertOpen(true);
  };

  const handleDeleteSessionConfirm = () => {
    setSessionAlertOpen(false);
    deleteSession(sessionToDelete.id)
      .then(() => {
        enqueueSnackbar("Session deleted!", { variant: "success" });
      })
      .catch((error) => {
        console.log(error);
        enqueueSnackbar("Failed to delete session!", { variant: "error" });
      });
  };

  const handleDeleteSessionCancel = () => {
    setSessionAlertOpen(false);
  };

  const handleSessionClick = (sessionCode) => {
    setRedirectToSessionCode(sessionCode);
  };

  return !isLoggedIn ? (
    <Redirect to="/" />
  ) : (
    <>
      {redirectToCreateQuiz && <Redirect push to="/quiz" />}
      {redirectToQuizId && <Redirect push to={`/quiz/${redirectToQuizId}`} />}
      {redirectToSessionCode && (
        <Redirect push to={`/control/${redirectToSessionCode}`} />
      )}
      <Page>
        <Container maxWidth="md" className={classes.container}>
          <Grid container spacing={1}>
            <Grid item xs={true}>
              <Typography variant="h4" color="textPrimary">
                Your Quizzes
              </Typography>
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                onClick={handleCreateQuizClick}
              >
                Create
              </Button>
            </Grid>
          </Grid>
        </Container>
        <Container maxWidth="md" className={classes.container}>
          {loading ? (
            <Spinner />
          ) : (
            <>
              {quizzes.length === 0 ? (
                <Typography variant="h5" align="center" color="textSecondary">
                  You haven't created any quizzes yet
                </Typography>
              ) : (
                <Grid container spacing={4} alignItems="flex-end">
                  {quizzes.map((quiz) => (
                    <Quiz
                      key={quiz.id}
                      quiz={quiz}
                      onScheduleSessionClick={handleScheduleSessionClick}
                      onEditClick={handleEditQuizClick}
                      onDeleteClick={handleDeleteQuizClick}
                      onEditSessionClick={handleEditSessionClick}
                      onDeleteSessionClick={handleDeleteSessionClick}
                      onSessionClick={handleSessionClick}
                    />
                  ))}
                </Grid>
              )}
            </>
          )}
        </Container>
      </Page>
      <AlertDialog
        title="Delete quiz?"
        message={`Are you sure you want to delete quiz ${quizToDelete?.title}?`}
        isOpen={quizAlertOpen}
        onConfirm={handleDeleteQuizConfirm}
        onClose={handleDeleteQuizCancel}
      />
      <AlertDialog
        title="Delete session?"
        message={`Are you sure you want to delete session ${sessionToDelete?.code}?`}
        isOpen={sessionAlertOpen}
        onConfirm={handleDeleteSessionConfirm}
        onClose={handleDeleteSessionCancel}
      />
      <ManageSessionDialog
        isOpen={isSessionDialogOpen}
        session={session}
        onChange={handleSessionChange}
        onSubmit={handleSessionSubmit}
        onCancel={handleSessionCancel}
        saving={savingSession}
        errors={sessionErrors}
      />
    </>
  );
};

QuizzesPage.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  quizzes: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  loadQuizzes: PropTypes.func.isRequired,
  deleteQuiz: PropTypes.func.isRequired,
  saveSession: PropTypes.func.isRequired,
  deleteSession: PropTypes.func.isRequired,
};

const mapStateToProps = ({ auth, quizzes, apiCallsInProgress }) => {
  return {
    isLoggedIn: auth.isLoggedIn,
    quizzes: quizzes.quizList,
    loading: apiCallsInProgress.loadingQuizzes,
  };
};

const mapDispatchToProps = {
  loadQuizzes: quizActions.loadQuizzes,
  deleteQuiz: quizActions.deleteQuiz,
  saveSession: sessionActions.saveSession,
  deleteSession: sessionActions.deleteSession,
};

export default connect(mapStateToProps, mapDispatchToProps)(QuizzesPage);
