import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Avatar,
  Box,
  CardContent,
  CardMedia,
  LinearProgress,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Tooltip,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  actions: {
    display: "block",
    "& > :not(:first-child)": {
      marginTop: 8,
    },
  },
  cardMedia: {
    objectFit: "scale-down",
    backgroundSize: "contain",
    paddingTop: "56.25%", // 16:9
  },
  correctAnswerAvatar: {
    color: "#fff",
    backgroundColor: theme.palette.success.main,
  },
}));

const normaliseProgress = (value, maxValue) =>
  maxValue ? (value * 100) / maxValue : 0;

const QuestionActivity = ({
  question,
  revealed,
  submissions,
  remainingTime,
}) => {
  const classes = useStyles();

  return (
    <>
      {((revealed && question.revealedImageUrl) || question.imageUrl) && (
        <CardMedia
          className={classes.cardMedia}
          image={
            revealed && question.revealedImageUrl
              ? question.revealedImageUrl
              : question.imageUrl
          }
          title="Question Image"
        />
      )}
      <CardContent className={classes.cardContent}>
        <Typography variant="h5" color="textPrimary" gutterBottom>
          {question.text}
        </Typography>
        <List>
          {question.answers.map((answer) => {
            const answerSubmissions = submissions
              .filter((submission) => submission.answerId === answer.id)
              .map((submission) => submission.participant.name);
            const avatar =
              answerSubmissions.length === 0 ? (
                <ListItemAvatar>
                  <Avatar
                    className={clsx(
                      answer.isCorrect ? classes.correctAnswerAvatar : null
                    )}
                  >
                    0
                  </Avatar>
                </ListItemAvatar>
              ) : (
                <ListItemAvatar>
                  <Tooltip
                    interactive
                    arrow
                    title={answerSubmissions.map((username) => (
                      <React.Fragment key={username}>
                        {username}
                        <br />
                      </React.Fragment>
                    ))}
                  >
                    <Avatar
                      className={clsx(
                        answer.isCorrect ? classes.correctAnswerAvatar : null
                      )}
                    >
                      {
                        submissions.filter(
                          (submission) => submission.answerId === answer.id
                        ).length
                      }
                    </Avatar>
                  </Tooltip>
                </ListItemAvatar>
              );

            return (
              <ListItem key={answer.id}>
                {revealed && avatar}
                <ListItemText primary={answer.text} />
              </ListItem>
            );
          })}
        </List>
        {question.time && (
          <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
              <LinearProgress
                variant="determinate"
                value={normaliseProgress(remainingTime, question.time)}
              />
            </Box>
            <Box>
              <Typography variant="body2">
                {Math.ceil(remainingTime)}
              </Typography>
            </Box>
          </Box>
        )}
      </CardContent>
    </>
  );
};

QuestionActivity.propTypes = {
  question: PropTypes.object.isRequired,
  revealed: PropTypes.bool.isRequired,
  submissions: PropTypes.array.isRequired,
  remainingTime: PropTypes.number.isRequired,
};

export default QuestionActivity;
