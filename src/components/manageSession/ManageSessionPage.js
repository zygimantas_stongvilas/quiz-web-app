import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useSnackbar } from "notistack";
import { Redirect } from "react-router";
import PropTypes from "prop-types";
import { HubConnectionBuilder } from "@microsoft/signalr";
import { Container, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import * as controlActions from "../../redux/actions/controlActions";
import activityType from "../../tools/activityType";
import Page from "../common/Page";
import QuizActivity from "./QuizActivity";
import QuizAgenda from "./QuizAgenda";
import Spinner from "../common/Spinner";
import { API_URL } from "../../api/apiUtils";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
  },
}));

const ManageSessionPage = ({
  isLoggedIn,
  sessionCode,
  session,
  agenda,
  currentQuestion,
  leaderboards,
  submissions,
  loading,
  loadingAgenda,
  loadingActivity,
  loadSession,
  loadAgenda,
  loadQuestion,
  loadLeaderboards,
  loadSubmissions,
  changeStatus,
  participantJoined,
}) => {
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();

  const [remainingTime, setRemainingTime] = useState(
    currentQuestion?.remainingTime ? currentQuestion.remainingTime : 0
  );
  const [timerStarted, setTimerStarted] = useState(false);

  const [connection, setConnection] = useState(null);

  useEffect(() => {
    if (!isLoggedIn) return;

    loadSession(sessionCode).catch((error) => {
      console.log(error);
      enqueueSnackbar("Failed to fetch session!", { variant: "error" });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!isLoggedIn) return;
    if (!session.id) return;

    loadAgenda(session.id).catch((error) => {
      console.log(error);
      enqueueSnackbar("Failed to fetch agenda!", { variant: "error" });
    });

    const newConnection = new HubConnectionBuilder()
      .withUrl(`${API_URL}/hubs/session?sessionId=${session.id}`)
      .withAutomaticReconnect()
      .build();

    setConnection(newConnection);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session.id]);

  useEffect(() => {
    if (connection) {
      connection
        .start()
        .then(() => connection.on("ReceiveParticipant", handleServerSideEvent))
        .catch((e) => console.log("Connection failed: ", e));
      return () => connection.off("ReceiveParticipant");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connection]);

  useEffect(() => {
    if (!isLoggedIn) return;
    if (!session.id) return;

    if (
      session.activity === activityType.JOINING_ENABLED ||
      session.activity === activityType.SHOW_LEADERBOARDS ||
      session.activity === activityType.FINISHED
    ) {
      loadLeaderboards(session.id).catch((error) => {
        console.log(error);
        enqueueSnackbar("Failed to fetch leaderboard!", { variant: "error" });
      });
    } else if (session.activity === activityType.QUESTION_ENABLED) {
      if (agenda.length === 0) return;
      loadQuestion(session.id)
        .then(() => {
          setTimerStarted(false);
        })
        .catch((error) => {
          console.log(error);
          enqueueSnackbar("Failed to fetch question!", { variant: "error" });
        });
    } else if (session.activity === activityType.QUESTION_DISABLED) {
      if (
        agenda.length === 0 ||
        session.currentQuestion === currentQuestion.order
      )
        return;
      loadQuestion(session.id).catch((error) => {
        console.log(error);
        enqueueSnackbar("Failed to fetch question!", { variant: "error" });
      });
    } else if (session.activity === activityType.QUESTION_REVEALED) {
      if (
        agenda.length > 0 &&
        session.currentQuestion !== currentQuestion.order
      ) {
        loadQuestion(session.id).catch((error) => {
          console.log(error);
          enqueueSnackbar("Failed to fetch question!", { variant: "error" });
        });
      }
      loadSubmissions(session.id).catch((error) => {
        console.log(error);
        enqueueSnackbar("Failed to fetch submissions!", { variant: "error" });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session.id, session.activity, session.currentQuestion, agenda.length]);

  useEffect(() => {
    if (!isLoggedIn) return;
    if (!currentQuestion?.time) return;
    if (!session.id) return;

    if (!timerStarted) {
      setRemainingTime(currentQuestion.remainingTime);
      setTimerStarted(true);
    } else {
      if (
        session.activity === activityType.QUESTION_ENABLED &&
        currentQuestion.remainingTime > 0
      ) {
        const timer = setInterval(() => {
          setRemainingTime((oldTime) => {
            if (oldTime <= 0) {
              clearInterval(timer);
              return 0;
            }
            return oldTime - 0.1;
          });
        }, 100);

        return () => clearInterval(timer);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session.id, currentQuestion, session.activity, timerStarted]);

  useEffect(() => {
    if (!isLoggedIn) return;
    if (!currentQuestion?.time) return;
    if (!session.id) return;

    if (
      session.activity === activityType.QUESTION_ENABLED &&
      remainingTime === 0
    ) {
      handleChangeStatusClick(
        activityType.QUESTION_DISABLED,
        session.currentQuestion
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session.id, remainingTime]);

  const handleServerSideEvent = (message) => {
    participantJoined(message);
  };

  const handleChangeStatusClick = (activity, currentQuestion) => {
    changeStatus(session.id, { activity, currentQuestion }).catch((error) => {
      console.log(error);
      enqueueSnackbar("Failed to change status!", { variant: "error" });
    });
  };

  return !isLoggedIn ? (
    <Redirect to="/" />
  ) : (
    <>
      <Page>
        <Container maxWidth="lg" className={classes.container}>
          {loading ? (
            <Spinner />
          ) : (
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                {loadingAgenda ? (
                  <Spinner />
                ) : (
                  <QuizAgenda
                    quiz={session.quiz}
                    agenda={agenda}
                    currentQuestion={session.currentQuestion}
                    currentActivity={session.activity}
                    onChangeActivityClick={handleChangeStatusClick}
                  />
                )}
              </Grid>
              <Grid item xs={12} md={6}>
                {loadingActivity ? (
                  <Spinner />
                ) : (
                  <QuizActivity
                    currentQuestion={currentQuestion}
                    currentActivity={session.activity}
                    leaderboards={leaderboards}
                    submissions={submissions}
                    remainingTime={
                      session.activity === activityType.QUESTION_ENABLED
                        ? remainingTime
                        : 0
                    }
                  />
                )}
              </Grid>
            </Grid>
          )}
        </Container>
      </Page>
    </>
  );
};

ManageSessionPage.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  sessionCode: PropTypes.string.isRequired,
  session: PropTypes.object.isRequired,
  agenda: PropTypes.array.isRequired,
  currentQuestion: PropTypes.object,
  leaderboards: PropTypes.array.isRequired,
  submissions: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  loadingAgenda: PropTypes.bool.isRequired,
  loadingActivity: PropTypes.bool.isRequired,
  loadSession: PropTypes.func.isRequired,
  loadAgenda: PropTypes.func.isRequired,
  loadQuestion: PropTypes.func.isRequired,
  loadLeaderboards: PropTypes.func.isRequired,
  loadSubmissions: PropTypes.func.isRequired,
  changeStatus: PropTypes.func.isRequired,
  participantJoined: PropTypes.func.isRequired,
};

const mapStateToProps = ({ auth, apiCallsInProgress, control }, ownProps) => {
  const sessionCode = ownProps.match.params.id;

  const loadingSession =
    apiCallsInProgress.loadingControlSession || !control.session.id;

  return {
    isLoggedIn: auth.isLoggedIn,
    sessionCode,
    session: control.session,
    agenda: control.agenda,
    currentQuestion: control.question,
    leaderboards: control.leaderboards,
    submissions: control.submissions,
    loading: loadingSession,
    loadingAgenda: loadingSession || apiCallsInProgress.loadingControlAgenda,
    loadingActivity:
      loadingSession ||
      apiCallsInProgress.loadingControlQuestion ||
      apiCallsInProgress.loadingControlSubmissions ||
      apiCallsInProgress.loadingControlLeaderboards,
  };
};

const mapDispatchToProps = {
  loadSession: controlActions.loadSession,
  loadAgenda: controlActions.loadAgenda,
  loadQuestion: controlActions.loadControlQuestion,
  loadLeaderboards: controlActions.loadControlLeaderboards,
  loadSubmissions: controlActions.loadControlQuestionSubmissions,
  changeStatus: controlActions.changeStatus,
  participantJoined: controlActions.sessionParticipantJoined,
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageSessionPage);
