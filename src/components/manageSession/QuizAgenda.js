import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Avatar,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
} from "@material-ui/core";
import EmojiEventsIcon from "@material-ui/icons/EmojiEvents";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import MeetingRoomIcon from "@material-ui/icons/MeetingRoom";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";
import { makeStyles } from "@material-ui/core/styles";
import activityType from "../../tools/activityType";

const useStyles = makeStyles((theme) => ({
  card: {
    // height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    // paddingTop: "56.25%", // 16:9
    height: 250,
  },
  cardContent: {
    flexGrow: 1,
  },
  currentItemAvatar: {
    color: theme.palette.getContrastText(theme.palette.primary.main),
    backgroundColor: theme.palette.primary.main,
  },
  currentDisabledItemAvatar: {
    color: theme.palette.getContrastText(theme.palette.error.main),
    backgroundColor: theme.palette.error.main,
  },
  currentRevealedItemAvatar: {
    color: "#fff",
    backgroundColor: theme.palette.success.main,
  },
}));

const QuizAgenda = ({
  quiz,
  agenda,
  currentQuestion,
  currentActivity,
  onChangeActivityClick,
}) => {
  const classes = useStyles();

  const getNextStatus = () => {
    if (currentActivity === activityType.WAITING_FOR_ACTIVATION) {
      return {
        activity: activityType.JOINING_ENABLED,
        currentQuestion: 0,
        label: "Open",
      };
    }
    if (currentActivity === activityType.JOINING_ENABLED) {
      const quizHasQuestions = agenda.length > 0;
      if (quizHasQuestions) {
        return {
          activity: activityType.QUESTION_ENABLED,
          currentQuestion: Math.min(
            ...agenda.map((question) => question.order)
          ),
          label: "Begin",
        };
      }
      return {
        activity: activityType.FINISHED,
        currentQuestion: 0,
        label: "Finish",
      };
    }
    if (currentActivity === activityType.QUESTION_ENABLED) {
      return {
        activity: activityType.QUESTION_DISABLED,
        currentQuestion,
        label: "Disable",
      };
    }
    if (currentActivity === activityType.QUESTION_DISABLED) {
      return {
        activity: activityType.QUESTION_REVEALED,
        currentQuestion,
        label: "Reveal",
      };
    }
    if (currentActivity === activityType.QUESTION_REVEALED) {
      if (agenda.length === 0) {
        return {
          currentActivity,
          currentQuestion,
          label: "Advance",
        };
      }
      const showLeaderboards = agenda.find(
        (question) => question.order === currentQuestion
      ).showLeaderboards;
      if (showLeaderboards) {
        return {
          activity: activityType.SHOW_LEADERBOARDS,
          currentQuestion,
          label: "Leaderboard",
        };
      }
      const nextQuestion = agenda.find(
        (question) => question.order > currentQuestion
      );
      if (nextQuestion) {
        return {
          activity: activityType.QUESTION_ENABLED,
          currentQuestion: nextQuestion.order,
          label: "Next Question",
        };
      }
      return {
        activity: activityType.FINISHED,
        currentQuestion,
        label: "Finish",
      };
    }
    if (currentActivity === activityType.SHOW_LEADERBOARDS) {
      if (agenda.length === 0) {
        return {
          currentActivity,
          currentQuestion,
          label: "Advance",
        };
      }
      const nextQuestion = agenda.find(
        (question) => question.order > currentQuestion
      );
      if (nextQuestion) {
        return {
          activity: activityType.QUESTION_ENABLED,
          currentQuestion: nextQuestion.order,
          label: "Next Question",
        };
      }
      return {
        activity: activityType.FINISHED,
        currentQuestion,
        label: "Finish",
      };
    }
  };

  const nextStatus = getNextStatus();

  return (
    <Card className={classes.card}>
      {quiz.imageUrl && (
        <CardMedia
          className={classes.cardMedia}
          image={quiz.imageUrl}
          title="Quiz Banner"
        />
      )}
      <CardContent className={classes.cardContent}>
        <Typography variant="h5" gutterBottom>
          {quiz.title}
        </Typography>
        <List>
          <ListItem divider>
            <ListItemAvatar>
              <Avatar
                className={clsx(
                  currentActivity === activityType.JOINING_ENABLED &&
                    classes.currentItemAvatar
                )}
              >
                <MeetingRoomIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Open" />
            {currentActivity !== activityType.JOINING_ENABLED && (
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  onClick={() =>
                    onChangeActivityClick(
                      activityType.JOINING_ENABLED,
                      agenda.length === 0
                        ? 0
                        : Math.min(...agenda.map((question) => question.order))
                    )
                  }
                >
                  <PlayCircleOutlineIcon />
                </IconButton>
              </ListItemSecondaryAction>
            )}
          </ListItem>
          {agenda.map((question, index) => (
            <div key={question.id}>
              <ListItem divider>
                <ListItemAvatar>
                  <Avatar
                    className={clsx(
                      currentQuestion === question.order
                        ? currentActivity === activityType.QUESTION_ENABLED
                          ? classes.currentItemAvatar
                          : currentActivity === activityType.QUESTION_DISABLED
                          ? classes.currentDisabledItemAvatar
                          : currentActivity === activityType.QUESTION_REVEALED
                          ? classes.currentRevealedItemAvatar
                          : null
                        : null
                    )}
                  >
                    {index + 1}
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={question.text}
                  secondary={question.isTimed ? `${question.time} s` : ""}
                />
                {(currentQuestion !== question.order ||
                  (currentActivity !== activityType.QUESTION_ENABLED &&
                    currentActivity !== activityType.QUESTION_DISABLED &&
                    currentActivity !== activityType.QUESTION_REVEALED)) && (
                  <ListItemSecondaryAction>
                    <IconButton
                      edge="end"
                      onClick={() =>
                        onChangeActivityClick(
                          activityType.QUESTION_ENABLED,
                          question.order
                        )
                      }
                    >
                      <PlayCircleOutlineIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                )}
              </ListItem>
              {question.showLeaderboards && (
                <ListItem divider>
                  <ListItemAvatar>
                    <Avatar
                      className={clsx(
                        currentQuestion === question.order &&
                          currentActivity === activityType.SHOW_LEADERBOARDS &&
                          classes.currentItemAvatar
                      )}
                    >
                      <EqualizerIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary="Leaderboard" />
                  {(currentQuestion !== question.order ||
                    currentActivity !== activityType.SHOW_LEADERBOARDS) && (
                    <ListItemSecondaryAction>
                      <IconButton
                        edge="end"
                        onClick={() =>
                          onChangeActivityClick(
                            activityType.SHOW_LEADERBOARDS,
                            question.order
                          )
                        }
                      >
                        <PlayCircleOutlineIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  )}
                </ListItem>
              )}
            </div>
          ))}
          <ListItem>
            <ListItemAvatar>
              <Avatar
                className={clsx(
                  currentActivity === activityType.FINISHED &&
                    classes.currentItemAvatar
                )}
              >
                <EmojiEventsIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Results" />
            {currentActivity !== activityType.FINISHED && (
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  onClick={() =>
                    onChangeActivityClick(
                      activityType.FINISHED,
                      agenda.length === 0
                        ? 0
                        : Math.max(...agenda.map((question) => question.order))
                    )
                  }
                >
                  <PlayCircleOutlineIcon />
                </IconButton>
              </ListItemSecondaryAction>
            )}
          </ListItem>
        </List>
      </CardContent>
      {nextStatus && (
        <CardActions>
          <Button
            fullWidth
            variant="contained"
            color="primary"
            onClick={() =>
              onChangeActivityClick(
                nextStatus.activity,
                nextStatus.currentQuestion
              )
            }
          >
            {nextStatus.label}
            {/* {currentActivity === activityType.WAITING_FOR_ACTIVATION
              ? "Open"
              : currentActivity === activityType.JOINING_ENABLED
              ? agenda.length > 0
                ? "Begin"
                : "Finish"
              : currentActivity === activityType.QUESTION_ENABLED
              ? "Disable"
              : currentActivity === activityType.QUESTION_DISABLED
              ? "Reveal"
              : currentActivity === activityType.QUESTION_REVEALED
              ? agenda.find((question) => question.order === currentQuestion)
                  .showLeaderboards
                ? "Leaderboard"
                : currentQuestion === agenda[agenda.length - 1].order
                ? "Finish"
                : "Next Question"
              : currentActivity === activityType.SHOW_LEADERBOARDS
              ? currentQuestion === agenda[agenda.length - 1].order
                ? "Finish"
                : "Next Question"
              : "Advance"} */}
          </Button>
        </CardActions>
      )}
    </Card>
  );
};

QuizAgenda.propTypes = {
  quiz: PropTypes.object.isRequired,
  agenda: PropTypes.array.isRequired,
  currentQuestion: PropTypes.number.isRequired,
  currentActivity: PropTypes.number.isRequired,
  onChangeActivityClick: PropTypes.func.isRequired,
};

export default QuizAgenda;
