import React from "react";
import PropTypes from "prop-types";
import { Card, CardContent, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import activityType from "../../tools/activityType";
import FinishedActivity from "../game/FinishedActivity";
import QuestionActivity from "./QuestionActivity";
import ShowLeaderboardsActivity from "../game/ShowLeaderboardsActivity";

const useStyles = makeStyles(() => ({
  card: {
    // height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardContent: {
    flexGrow: 1,
  },
  gridContainer: {
    height: "100%",
  },
}));

const QuizActivity = ({
  currentQuestion,
  currentActivity,
  leaderboards,
  submissions,
  remainingTime,
}) => {
  const classes = useStyles();

  return currentActivity !== activityType.FINISHED ? (
    <Card className={classes.card}>
      {currentActivity === activityType.WAITING_FOR_ACTIVATION ? (
        <CardContent className={classes.cardContent}>
          <Grid className={classes.gridContainer} container alignItems="center">
            <Grid item xs={12}>
              <Typography variant="h5" color="textPrimary" align="center">
                The session has not been started yet!
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      ) : currentActivity === activityType.JOINING_ENABLED ? (
        <ShowLeaderboardsActivity leaderboards={leaderboards} />
      ) : (currentActivity === activityType.QUESTION_ENABLED ||
          currentActivity === activityType.QUESTION_DISABLED ||
          currentActivity === activityType.QUESTION_REVEALED) &&
        currentQuestion.id ? (
        <QuestionActivity
          question={currentQuestion}
          // disabled={currentActivity === activityType.QUESTION_DISABLED}
          revealed={currentActivity === activityType.QUESTION_REVEALED}
          submissions={submissions}
          remainingTime={remainingTime}
        />
      ) : currentActivity === activityType.SHOW_LEADERBOARDS ? (
        <ShowLeaderboardsActivity showPoints leaderboards={leaderboards} />
      ) : null}
    </Card>
  ) : (
    <FinishedActivity leaderboards={leaderboards} />
  );
};

QuizActivity.propTypes = {
  currentQuestion: PropTypes.object.isRequired,
  currentActivity: PropTypes.number.isRequired,
  leaderboards: PropTypes.array.isRequired,
  submissions: PropTypes.array.isRequired,
  remainingTime: PropTypes.number.isRequired,
};

export default QuizActivity;
