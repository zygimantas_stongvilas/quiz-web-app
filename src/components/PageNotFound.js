import React from "react";
import { Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Page from "./common/Page";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
  },
}));

const PageNotFound = () => {
  const classes = useStyles();

  return (
    <Page>
      <Container maxWidth="sm" className={classes.container}>
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="textPrimary"
        >
          Page not found!
        </Typography>
      </Container>
    </Page>
  );
};

export default PageNotFound;
