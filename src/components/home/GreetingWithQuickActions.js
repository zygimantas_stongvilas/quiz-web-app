import React from "react";
import PropTypes from "prop-types";
import {
  Button,
  CircularProgress,
  Grid,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    margin: theme.spacing(1),
    position: "relative",
  },
  progress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  container: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
  },
}));

const GreetingWithQuickActions = ({
  joinSessionCode,
  onJoinSessionCodeChange,
  onJoinClick,
  joining = false,
  errors = {},
}) => {
  const classes = useStyles();

  return (
    <>
      <Typography variant="h2" align="center" color="textPrimary" gutterBottom>
        Welcome to QVizma!
      </Typography>
      <Typography variant="h5" align="center" color="textSecondary" paragraph>
        Here you can participate in a quiz together with your friends by
        entering an invitation code or joining a session from the list below
      </Typography>
      <div className={classes.heroButtons}>
        <form onSubmit={onJoinClick} autoComplete="off" noValidate>
          <Grid container spacing={2} justify="center" alignItems="center">
            <Grid item xs={12} sm="auto">
              <TextField
                error={!!errors.code}
                helperText={errors.code}
                label="Quiz Code"
                fullWidth
                required
                value={joinSessionCode}
                onChange={onJoinSessionCodeChange}
              />
            </Grid>
            <Grid item xs={12} sm="auto">
              <div className={classes.wrapper}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={joining}
                  fullWidth
                >
                  Join
                </Button>
                {joining && (
                  <CircularProgress className={classes.progress} size={24} />
                )}
              </div>
            </Grid>
          </Grid>
        </form>
      </div>
    </>
  );
};

GreetingWithQuickActions.propTypes = {
  joinSessionCode: PropTypes.string.isRequired,
  onJoinSessionCodeChange: PropTypes.func.isRequired,
  onJoinClick: PropTypes.func.isRequired,
  joining: PropTypes.bool,
  errors: PropTypes.object,
};

export default GreetingWithQuickActions;
