import React from "react";
import PropTypes from "prop-types";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Moment from "react-moment";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
}));

const SessionsList = ({ sessions, onJoinSessionClick }) => {
  const classes = useStyles();

  return (
    <Grid container spacing={4}>
      {sessions.map((session) => (
        <Grid item key={session.id} xs={12} sm={6} md={4}>
          <Card className={classes.card}>
            {session.quiz.imageUrl && (
              <CardMedia
                className={classes.cardMedia}
                image={session.quiz.imageUrl}
                title="Quiz Banner"
              />
            )}
            <CardContent className={classes.cardContent}>
              <Typography gutterBottom variant="h5">
                {session.quiz.title}
              </Typography>
              <Typography gutterBottom variant="body1">
                {session.code}
              </Typography>
              <Typography>{session.quiz.description}</Typography>
              <Typography variant="body2" color="textSecondary">
                {session.quiz.questionsCount} question
                {session.quiz.questionsCount === 1 ? "" : "s"}
              </Typography>
              <Typography variant="body2" color="textSecondary">
                <Moment format="YYYY-MM-DD HH:mm" interval={0}>
                  {new Date(session.scheduledTime)}
                </Moment>
              </Typography>
            </CardContent>
            <CardActions>
              <Button
                color="primary"
                onClick={() => onJoinSessionClick(session.code)}
              >
                Join
              </Button>
            </CardActions>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

SessionsList.propTypes = {
  sessions: PropTypes.array.isRequired,
  onJoinSessionClick: PropTypes.func.isRequired,
};

export default SessionsList;
