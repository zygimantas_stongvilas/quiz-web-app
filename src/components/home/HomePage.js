import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useSnackbar } from "notistack";
import PropTypes from "prop-types";
import { Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import * as gameActions from "../../redux/actions/gameActions";
import * as sessionActions from "../../redux/actions/upcomingSessionsActions";
import GreetingWithQuickActions from "./GreetingWithQuickActions";
import Page from "../common/Page";
import SessionsList from "./SessionsList";
import Spinner from "../common/Spinner";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
  },
}));

const HomePage = ({
  sessions,
  loading,
  loadSessions,
  loadSession,
  history,
}) => {
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();

  const [joinSessionCode, setJoinSessionCode] = useState("");

  const [errors, setErrors] = useState({});
  const [joining, setJoining] = useState(false);

  useEffect(() => {
    loadSessions().catch((error) => {
      console.log(error);
      enqueueSnackbar("Failed to fetch upcoming sessions!", {
        variant: "error",
      });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleJoinSessionCodeChange = (event) => {
    setJoinSessionCode(event.target.value);
  };

  const handleJoinClick = (event) => {
    event.preventDefault();
    setErrors({});

    const codeIsValid = () => {
      const errors = {};

      if (!joinSessionCode) errors.code = "Code is required.";
      else if (!/^[a-zA-Z0-9]+$/.test(joinSessionCode))
        errors.code = "Code can contain only letters and digits.";

      setErrors(errors);
      return Object.keys(errors).length === 0;
    };

    if (codeIsValid()) {
      setJoining(true);
      loadSession(joinSessionCode)
        .then(() => {
          history.push(`/play/${joinSessionCode}`);
        })
        .catch((error) => {
          console.log(error);
          setErrors({ code: "Session doesn't exist." });
          setJoining(false);
        });
    }
  };

  const handleJoinSessionClick = (sessionCode) => {
    history.push(`/play/${sessionCode}`);
  };

  return (
    <Page>
      <Container maxWidth="sm" className={classes.container}>
        <GreetingWithQuickActions
          joinSessionCode={joinSessionCode}
          onJoinSessionCodeChange={handleJoinSessionCodeChange}
          onJoinClick={handleJoinClick}
          joining={joining}
          errors={errors}
        />
      </Container>
      <Container maxWidth="md" className={classes.container}>
        {loading ? (
          <Spinner />
        ) : sessions.length === 0 ? (
          <Typography variant="h5" align="center" color="textPrimary">
            Looks like there are no sessions scheduled!
          </Typography>
        ) : (
          <SessionsList
            sessions={sessions}
            onJoinSessionClick={handleJoinSessionClick}
          />
        )}
      </Container>
    </Page>
  );
};

HomePage.propTypes = {
  sessions: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  loadSessions: PropTypes.func.isRequired,
  loadSession: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = ({ upcomingSessions, apiCallsInProgress }) => ({
  sessions: upcomingSessions,
  loading: apiCallsInProgress.loadingUpcomingSessions,
});

const mapDispatchToProps = {
  loadSessions: sessionActions.loadUpcomingSessions,
  loadSession: gameActions.loadSession,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
