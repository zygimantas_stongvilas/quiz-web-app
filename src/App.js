import React from "react";
import { Route, Switch } from "react-router-dom";
import { CssBaseline } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import "moment-timezone";
import GamePage from "./components/game/GamePage";
import Header from "./components/common/Header";
import HomePage from "./components/home/HomePage";
import ManageQuizPage from "./components/manageQuiz/ManageQuizPage";
import ManageSessionPage from "./components/manageSession/ManageSessionPage";
import PageNotFound from "./components/PageNotFound";
import QuizzesPage from "./components/quizzes/QuizzesPage";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
  },
}));

const App = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/quizzes" component={QuizzesPage} />
        <Route path="/quiz/:id" component={ManageQuizPage} />
        <Route path="/quiz" component={ManageQuizPage} />
        <Route path="/control/:id" component={ManageSessionPage} />
        <Route path="/play/:id" component={GamePage} />
        <Route component={PageNotFound} />
      </Switch>
    </div>
  );
};

export default App;
