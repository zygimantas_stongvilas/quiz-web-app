import * as types from "./actionTypes";

export const beginApiCall = () => ({
  type: types.BEGIN_API_CALL,
});

export const anonymousCallSuccess = () => ({
  type: types.ANONYMOUS_CALL_SUCCESS,
});

export const apiCallError = (error) => ({
  type: types.API_CALL_ERROR,
  error,
});
