import * as types from "./actionTypes";
import * as sessionApi from "../../api/sessionApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export const sessionParticipantJoined = (participant) => ({
  type: types.SESSION_PARTICIPANT_JOINED,
  participant,
});

export const loadSessionStart = () => ({
  type: types.LOAD_CONTROL_SESSION_START,
});

export const loadSessionSuccess = (session) => ({
  type: types.LOAD_CONTROL_SESSION_SUCCESS,
  session,
});

export const loadSessionError = (error) => ({
  type: types.LOAD_CONTROL_SESSION_ERROR,
  error,
});

export const loadAgendaStart = () => ({
  type: types.LOAD_AGENDA_START,
});

export const loadAgendaSuccess = (agenda) => ({
  type: types.LOAD_AGENDA_SUCCESS,
  agenda,
});

export const loadAgendaError = (error) => ({
  type: types.LOAD_AGENDA_ERROR,
  error,
});

export const loadControlQuestionStart = () => ({
  type: types.LOAD_CONTROL_QUESTION_START,
});

export const loadControlQuestionSuccess = (question) => ({
  type: types.LOAD_CONTROL_QUESTION_SUCCESS,
  question,
});

export const loadControlQuestionError = (error) => ({
  type: types.LOAD_CONTROL_QUESTION_ERROR,
  error,
});

export const loadSubmissionsStart = () => ({
  type: types.LOAD_SUBMISSIONS_START,
});

export const loadSubmissionsSuccess = (submissions) => ({
  type: types.LOAD_SUBMISSIONS_SUCCESS,
  submissions,
});

export const loadSubmissionsError = (error) => ({
  type: types.LOAD_SUBMISSIONS_ERROR,
  error,
});

export const loadLeaderboardsStart = () => ({
  type: types.LOAD_CONTROL_LEADERBOARDS_START,
});

export const loadControlLeaderboardsSuccess = (leaderboards) => ({
  type: types.LOAD_CONTROL_LEADERBOARDS_SUCCESS,
  leaderboards,
});

export const loadLeaderboardsError = (error) => ({
  type: types.LOAD_CONTROL_LEADERBOARDS_ERROR,
  error,
});

export const changeActivitySuccess = (changedActivity) => ({
  type: types.CHANGE_ACTIVITY_SUCCESS,
  changedActivity,
});

export const changeStatusSuccess = (changedStatus) => ({
  type: types.CHANGE_STATUS_SUCCESS,
  changedStatus,
});

export const loadSession = (sessionCode) => (dispatch) => {
  dispatch(loadSessionStart());
  return sessionApi
    .loadSession(sessionCode)
    .then((session) => dispatch(loadSessionSuccess(session)))
    .catch((error) => {
      dispatch(loadSessionError(error));
      throw error;
    });
};

export const loadAgenda = (sessionId) => (dispatch) => {
  dispatch(loadAgendaStart());
  return sessionApi
    .loadAgenda(sessionId)
    .then((agenda) => dispatch(loadAgendaSuccess(agenda)))
    .catch((error) => {
      dispatch(loadAgendaError(error));
      throw error;
    });
};

export const loadControlQuestion = (sessionId) => (dispatch) => {
  dispatch(loadControlQuestionStart());
  return sessionApi
    .loadCurrentQuestion(sessionId)
    .then((question) => dispatch(loadControlQuestionSuccess(question)))
    .catch((error) => {
      dispatch(loadControlQuestionError(error));
      throw error;
    });
};

export const loadControlQuestionSubmissions = (sessionId) => (dispatch) => {
  dispatch(loadSubmissionsStart());
  return sessionApi
    .loadCurrentQuestionSubmissions(sessionId)
    .then((submissions) => dispatch(loadSubmissionsSuccess(submissions)))
    .catch((error) => {
      dispatch(loadSubmissionsError(error));
      throw error;
    });
};

export const loadControlLeaderboards = (sessionId) => (dispatch) => {
  dispatch(loadLeaderboardsStart());
  return sessionApi
    .loadLeaderboards(sessionId)
    .then((leaderboards) =>
      dispatch(loadControlLeaderboardsSuccess(leaderboards))
    )
    .catch((error) => {
      dispatch(loadLeaderboardsError(error));
      throw error;
    });
};

export const changeActivity = (sessionId) => (dispatch) => {
  dispatch(beginApiCall());
  return sessionApi
    .changeActivity(sessionId)
    .then((changedActivity) => dispatch(changeActivitySuccess(changedActivity)))
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};

export const changeStatus = (sessionId, status) => (dispatch) => {
  dispatch(beginApiCall());
  return sessionApi
    .changeStatus(sessionId, status)
    .then((changedStatus) => dispatch(changeStatusSuccess(changedStatus)))
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};
