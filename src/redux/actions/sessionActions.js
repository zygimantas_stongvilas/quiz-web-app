import * as types from "./actionTypes";
import * as sessionApi from "../../api/sessionApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export const createSessionSuccess = (session) => ({
  type: types.CREATE_SESSION_SUCCESS,
  session,
});

export const updateSessionSuccess = (session) => ({
  type: types.UPDATE_SESSION_SUCCESS,
  session,
});

export const deleteSessionSuccess = (sessionId) => ({
  type: types.DELETE_SESSION_SUCCESS,
  sessionId,
});

export const saveSession = (session) => (dispatch) => {
  dispatch(beginApiCall());
  return sessionApi
    .saveSession(session)
    .then((savedSession) => {
      session.id
        ? dispatch(updateSessionSuccess(session))
        : dispatch(createSessionSuccess(savedSession));
    })
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};

export const deleteSession = (sessionId) => (dispatch) => {
  dispatch(beginApiCall());
  return sessionApi
    .deleteSession(sessionId)
    .then(() => dispatch(deleteSessionSuccess(sessionId)))
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};
