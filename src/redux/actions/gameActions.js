import * as types from "./actionTypes";
import * as sessionApi from "../../api/sessionApi";
import {
  beginApiCall,
  anonymousCallSuccess,
  apiCallError,
} from "./apiStatusActions";

export const sessionStatusChanged = (sessionId, status) => ({
  type: types.SESSION_STATUS_CHANGED,
  sessionId,
  status,
});

export const loadSessionStart = () => ({
  type: types.LOAD_SESSION_START,
});

export const loadSessionSuccess = (session) => ({
  type: types.LOAD_SESSION_SUCCESS,
  session,
});

export const loadSessionError = (error) => ({
  type: types.LOAD_SESSION_ERROR,
  error,
});

export const joinGameSuccess = (participant) => ({
  type: types.JOIN_GAME_SUCCESS,
  participant,
});

export const loadCurrentQuestionStart = () => ({
  type: types.LOAD_CURRENT_QUESTION_START,
});

export const loadCurrentQuestionSuccess = (question) => ({
  type: types.LOAD_CURRENT_QUESTION_SUCCESS,
  question,
});

export const loadCurrentQuestionError = (error) => ({
  type: types.LOAD_CURRENT_QUESTION_ERROR,
  error,
});

export const loadLeaderboardsStart = () => ({
  type: types.LOAD_LEADERBOARDS_START,
});

export const loadLeaderboardsSuccess = (leaderboards) => ({
  type: types.LOAD_LEADERBOARDS_SUCCESS,
  leaderboards,
});

export const loadLeaderboardsError = (error) => ({
  type: types.LOAD_LEADERBOARDS_ERROR,
  error,
});

export const loadSession = (sessionCode) => (dispatch) => {
  dispatch(loadSessionStart());
  return sessionApi
    .loadSession(sessionCode)
    .then((session) => dispatch(loadSessionSuccess(session)))
    .catch((error) => {
      dispatch(loadSessionError(error));
      throw error;
    });
};

export const joinGame = (sessionId, name) => (dispatch) => {
  dispatch(beginApiCall());
  return sessionApi
    .joinSession(sessionId, name)
    .then((participant) => dispatch(joinGameSuccess(participant)))
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};

export const loadCurrentQuestion = (sessionId) => (dispatch) => {
  dispatch(loadCurrentQuestionStart());
  return sessionApi
    .loadCurrentQuestion(sessionId)
    .then((question) => dispatch(loadCurrentQuestionSuccess(question)))
    .catch((error) => {
      dispatch(loadCurrentQuestionError(error));
      throw error;
    });
};

export const submitAnswer = (sessionId, answer) => (dispatch) => {
  dispatch(beginApiCall());
  return sessionApi
    .submitAnswer(sessionId, answer)
    .then(() => dispatch(anonymousCallSuccess()))
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};

export const loadLeaderboards = (sessionId) => (dispatch) => {
  dispatch(loadLeaderboardsStart());
  return sessionApi
    .loadLeaderboards(sessionId)
    .then((leaderboards) => dispatch(loadLeaderboardsSuccess(leaderboards)))
    .catch((error) => {
      dispatch(loadLeaderboardsError(error));
      throw error;
    });
};
