import * as types from "./actionTypes";
import * as quizApi from "../../api/quizApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export const loadQuizzesStart = () => ({
  type: types.LOAD_QUIZZES_START,
});

export const loadQuizzesSuccess = (quizzes) => ({
  type: types.LOAD_QUIZZES_SUCCESS,
  quizzes,
});

export const loadQuizzesError = (error) => ({
  type: types.LOAD_QUIZZES_ERROR,
  error,
});

export const loadQuizStart = () => ({
  type: types.LOAD_QUIZ_START,
});

export const loadQuizSuccess = (quiz) => ({
  type: types.LOAD_QUIZ_SUCCESS,
  quiz,
});

export const loadQuizError = (error) => ({
  type: types.LOAD_QUIZ_ERROR,
  error,
});

export const createQuizSuccess = (quiz) => ({
  type: types.CREATE_QUIZ_SUCCESS,
  quiz,
});

export const updateQuizSuccess = (quiz) => ({
  type: types.UPDATE_QUIZ_SUCCESS,
  quiz,
});

export const deleteQuizSuccess = (quizId) => ({
  type: types.DELETE_QUIZ_SUCCESS,
  quizId,
});

export const loadQuizzes = () => (dispatch) => {
  dispatch(loadQuizzesStart());
  return quizApi
    .getQuizzes()
    .then((quizzes) => dispatch(loadQuizzesSuccess(quizzes)))
    .catch((error) => {
      dispatch(loadQuizzesError(error));
      throw error;
    });
};

export const loadQuiz = (quizId) => (dispatch) => {
  dispatch(loadQuizStart());
  return quizApi
    .getQuiz(quizId)
    .then((loadedQuiz) => dispatch(loadQuizSuccess(loadedQuiz)))
    .catch((error) => {
      dispatch(loadQuizError(error));
      throw error;
    });
};

export const saveQuiz = (quiz) => (dispatch) => {
  dispatch(beginApiCall());
  return quizApi
    .saveQuiz(quiz)
    .then((savedQuiz) => {
      quiz.id
        ? dispatch(updateQuizSuccess(quiz))
        : dispatch(createQuizSuccess(savedQuiz));
    })
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};

export const deleteQuiz = (quizId) => (dispatch) => {
  dispatch(beginApiCall());
  return quizApi
    .deleteQuiz(quizId)
    .then(() => dispatch(deleteQuizSuccess(quizId)))
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};
