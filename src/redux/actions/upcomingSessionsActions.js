import * as types from "./actionTypes";
import * as sessionApi from "../../api/sessionApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export const loadUpcomingSessionsStart = () => ({
  type: types.LOAD_UPCOMING_SESSIONS_START,
});

export const loadUpcomingSessionsSuccess = (sessions) => ({
  type: types.LOAD_UPCOMING_SESSIONS_SUCCESS,
  sessions,
});

export const loadUpcomingSessionsError = (error) => ({
  type: types.LOAD_UPCOMING_SESSIONS_ERROR,
  error,
});

export const createSessionSuccess = (session) => ({
  type: types.CREATE_SESSION_SUCCESS,
  session,
});

export const loadUpcomingSessions = () => (dispatch) => {
  dispatch(loadUpcomingSessionsStart());
  return sessionApi
    .getSessions()
    .then((sessions) => dispatch(loadUpcomingSessionsSuccess(sessions)))
    .catch((error) => {
      dispatch(
        loadUpcomingSessionsError(error, types.LOAD_UPCOMING_SESSIONS_ERROR)
      );
      throw error;
    });
};

export const saveSession = (session) => (dispatch) => {
  dispatch(beginApiCall());
  return sessionApi
    .saveSession(session)
    .then((savedSessionId) => {
      dispatch(createSessionSuccess({ ...session, id: savedSessionId }));
      // session.id
      //   ? dispatch(updateSessionSuccess(savedSession))
      //   : dispatch(createSessionSuccess(savedSession));
    })
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};
