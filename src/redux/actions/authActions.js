import * as types from "./actionTypes";
import * as authApi from "../../api/authApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export const loginSuccess = (userData) => ({
  type: types.LOGIN_SUCCESS,
  userData,
});

export const logoutSuccess = () => ({
  type: types.LOGOUT_SUCCESS,
});

export const loadUserStart = () => ({
  type: types.LOAD_USER_START,
});

export const loadUserSuccess = (userData) => ({
  type: types.LOAD_USER_SUCCESS,
  userData,
});

export const loadUserError = (error) => ({
  type: types.LOAD_USER_ERROR,
  error,
});

export const login = (googleLoginResponse) => (dispatch) => {
  dispatch(beginApiCall());
  return authApi
    .login(googleLoginResponse)
    .then((userData) => dispatch(loginSuccess(userData)))
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};

export const logout = () => (dispatch) => {
  dispatch(beginApiCall());
  return authApi
    .logout()
    .then(() => dispatch(logoutSuccess()))
    .catch((error) => {
      dispatch(apiCallError(error));
      throw error;
    });
};

export const loadUser = () => (dispatch) => {
  dispatch(loadUserStart());
  return authApi
    .loadUser()
    .then((userData) => dispatch(loadUserSuccess(userData)))
    .catch((error) => {
      dispatch(loadUserError(error));
      throw error;
    });
};
