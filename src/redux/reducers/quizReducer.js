import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const quizReducer = (state = initialState.quizzes, action) => {
  switch (action.type) {
    case types.LOAD_QUIZZES_SUCCESS:
      return { ...state, quizList: action.quizzes };
    case types.LOAD_QUIZ_SUCCESS:
      return { ...state, activeQuizDetails: action.quiz };
    case types.CREATE_QUIZ_SUCCESS:
      return {
        ...state,
        quizList: [
          ...state.quizList,
          {
            ...action.quiz,
            questionsCount: action.quiz.questions.length,
            sessions: action.quiz.sessions || [],
          },
        ],
      };
    case types.UPDATE_QUIZ_SUCCESS:
      return {
        ...state,
        quizList: state.quizList.map((quiz) =>
          quiz.id === action.quiz.id
            ? {
                ...action.quiz,
                questionsCount: action.quiz.questions.length,
                sessions: action.quiz.sessions || [],
              }
            : quiz
        ),
      };
    case types.DELETE_QUIZ_SUCCESS:
      return {
        ...state,
        quizList: state.quizList.filter((quiz) => quiz.id !== action.quizId),
      };
    case types.CREATE_SESSION_SUCCESS:
      return {
        ...state,
        quizList: state.quizList.map((quiz) =>
          quiz.id === action.session.quizId
            ? {
                ...quiz,
                sessions: [...quiz.sessions, { ...action.session }],
              }
            : quiz
        ),
      };
    case types.UPDATE_SESSION_SUCCESS:
      return {
        ...state,
        quizList: state.quizList.map((quiz) =>
          quiz.id === action.session.quizId
            ? {
                ...quiz,
                sessions: quiz.sessions.map((session) =>
                  session.id === action.session.id
                    ? { ...action.session }
                    : session
                ),
              }
            : quiz
        ),
      };
    case types.DELETE_SESSION_SUCCESS:
      return {
        ...state,
        quizList: state.quizList.map((quiz) =>
          quiz.sessions.some((session) => session.id === action.sessionId)
            ? {
                ...quiz,
                sessions: quiz.sessions.filter(
                  (session) => session.id !== action.sessionId
                ),
              }
            : quiz
        ),
      };
    default:
      return state;
  }
};

export default quizReducer;
