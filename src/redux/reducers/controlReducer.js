import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const controlReducer = (state = initialState.control, action) => {
  switch (action.type) {
    case types.SESSION_PARTICIPANT_JOINED:
      return {
        ...state,
        leaderboards: [action.participant, ...state.leaderboards],
      };
    case types.LOAD_CONTROL_SESSION_SUCCESS:
      return {
        ...state,
        session: action.session,
      };
    case types.LOAD_AGENDA_SUCCESS:
      return {
        ...state,
        agenda: action.agenda,
      };
    case types.LOAD_CONTROL_QUESTION_SUCCESS:
      return {
        ...state,
        question: action.question,
      };
    case types.LOAD_SUBMISSIONS_SUCCESS:
      return {
        ...state,
        submissions: action.submissions,
      };
    case types.LOAD_CONTROL_LEADERBOARDS_SUCCESS:
      return {
        ...state,
        leaderboards: action.leaderboards,
      };
    case types.CHANGE_ACTIVITY_SUCCESS:
      return {
        ...state,
        session: {
          ...state.session,
          activity: action.changedActivity.activity,
          currentQuestion: action.changedActivity.currentQuestion,
        },
      };
    case types.CHANGE_STATUS_SUCCESS:
      return {
        ...state,
        session: {
          ...state.session,
          activity: action.changedStatus.activity,
          currentQuestion: action.changedStatus.currentQuestion,
        },
      };
    case types.DELETE_SESSION_SUCCESS:
      return state.session.id === action.sessionId
        ? initialState.control
        : state;
    default:
      return state;
  }
};

export default controlReducer;
