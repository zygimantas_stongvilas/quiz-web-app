import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const gameReducer = (state = initialState.game, action) => {
  switch (action.type) {
    case types.SESSION_STATUS_CHANGED:
      return {
        ...state,
        sessionDetails:
          state.sessionDetails.id === action.sessionId
            ? {
                ...state.sessionDetails,
                activity: action.status.activity,
                currentQuestion: action.status.currentQuestion,
              }
            : {},
      };
    case types.LOAD_SESSION_SUCCESS:
      const participant = localStorage.getItem(action.session.id);
      return {
        ...state,
        sessionDetails: action.session,
        participant: participant ? JSON.parse(participant) : state.participant,
      };
    case types.JOIN_GAME_SUCCESS:
      localStorage.setItem(
        action.participant.sessionId,
        JSON.stringify(action.participant)
      );
      return { ...state, participant: action.participant };
    case types.LOAD_CURRENT_QUESTION_SUCCESS:
      return { ...state, question: action.question };
    case types.LOAD_LEADERBOARDS_SUCCESS:
      return { ...state, leaderboards: action.leaderboards };
    default:
      return state;
  }
};

export default gameReducer;
