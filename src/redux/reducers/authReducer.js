import * as types from "../actions/actionTypes";
import initialState from "./initialState";
import UnauthorizedError from "../../api/errors/unauthorizedError";

const authReducer = (state = initialState.auth, action) => {
  switch (action.type) {
    case types.LOGIN_SUCCESS:
      localStorage.setItem("token", action.userData.token);
      return { isLoggedIn: true, userData: action.userData };
    case types.LOGOUT_SUCCESS:
      localStorage.removeItem("token");
      return { isLoggedIn: false, userData: {} };
    case types.LOAD_USER_SUCCESS:
      return { ...state, userData: action.userData };
    case types.LOAD_USER_ERROR:
    case types.API_CALL_ERROR:
      if (action.error instanceof UnauthorizedError) {
        localStorage.removeItem("token");
        return { isLoggedIn: false, userData: {} };
      }
      return state;
    default:
      return state;
  }
};

export default authReducer;
