import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const upcomingSessionsReducer = (
  state = initialState.upcomingSessions,
  action
) => {
  switch (action.type) {
    case types.LOAD_UPCOMING_SESSIONS_SUCCESS:
      return action.sessions;
    default:
      return state;
  }
};

export default upcomingSessionsReducer;
