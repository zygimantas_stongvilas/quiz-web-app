import { combineReducers } from "redux";
import apiStatusReducer from "./apiStatusReducer";
import authReducer from "./authReducer";
import controlReducer from "./controlReducer";
import gameReducer from "./gameReducer";
import quizReducer from "./quizReducer";
import upcomingSessionsReducer from "./upcomingSessionsReducer";

const rootReducer = combineReducers({
  apiCallsInProgress: apiStatusReducer,
  auth: authReducer,
  control: controlReducer,
  game: gameReducer,
  quizzes: quizReducer,
  upcomingSessions: upcomingSessionsReducer,
});

export default rootReducer;
