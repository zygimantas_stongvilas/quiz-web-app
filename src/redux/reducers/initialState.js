// eslint-disable-next-line import/no-anonymous-default-export
export default {
  apiCallsInProgress: {
    callsCount: 0,
    loadingUser: false,
    loadingUpcomingSessions: false,
    loadingQuizzes: false,
    loadingQuiz: false,
    loadingControlSession: false,
    loadingControlAgenda: false,
    loadingControlQuestion: false,
    loadingControlSubmissions: false,
    loadingControlLeaderboards: false,
    loadingGameSession: false,
    loadingGameQuestion: false,
    loadingGameLeaderboards: false,
  },
  auth: {
    isLoggedIn: Boolean(localStorage.getItem("token")),
    userData: {},
  },
  control: {
    session: {},
    agenda: [],
    question: {},
    leaderboards: [],
    submissions: [],
  },
  game: {
    sessionDetails: {},
    participant: {},
    question: {},
    leaderboards: [],
  },
  quizzes: {
    quizList: [],
    activeQuizDetails: {},
  },
  upcomingSessions: [],
};
