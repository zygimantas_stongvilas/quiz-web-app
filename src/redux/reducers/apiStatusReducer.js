import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const actionTypeEndsInSuccess = (type) =>
  type.substring(type.length - 8) === "_SUCCESS";

const apiStatusReducer = (state = initialState.apiCallsInProgress, action) => {
  switch (action.type) {
    case types.LOAD_USER_START:
      return {
        ...state,
        loadingUser: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_USER_SUCCESS:
    case types.LOAD_USER_ERROR:
      return {
        ...state,
        loadingUser: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_UPCOMING_SESSIONS_START:
      return {
        ...state,
        loadingUpcomingSessions: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_UPCOMING_SESSIONS_SUCCESS:
    case types.LOAD_UPCOMING_SESSIONS_ERROR:
      return {
        ...state,
        loadingUpcomingSessions: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_QUIZZES_START:
      return {
        ...state,
        loadingQuizzes: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_QUIZZES_SUCCESS:
    case types.LOAD_QUIZZES_ERROR:
      return {
        ...state,
        loadingQuizzes: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_QUIZ_START:
      return {
        ...state,
        loadingQuiz: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_QUIZ_SUCCESS:
    case types.LOAD_QUIZ_ERROR:
      return {
        ...state,
        loadingQuiz: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_CONTROL_SESSION_START:
      return {
        ...state,
        loadingControlSession: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_CONTROL_SESSION_SUCCESS:
    case types.LOAD_CONTROL_SESSION_ERROR:
      return {
        ...state,
        loadingControlSession: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_AGENDA_START:
      return {
        ...state,
        loadingControlAgenda: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_AGENDA_SUCCESS:
    case types.LOAD_AGENDA_ERROR:
      return {
        ...state,
        loadingControlAgenda: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_CONTROL_QUESTION_START:
      return {
        ...state,
        loadingControlQuestion: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_CONTROL_QUESTION_SUCCESS:
    case types.LOAD_CONTROL_QUESTION_ERROR:
      return {
        ...state,
        loadingControlQuestion: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_SUBMISSIONS_START:
      return {
        ...state,
        loadingControlSubmissions: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_SUBMISSIONS_SUCCESS:
    case types.LOAD_SUBMISSIONS_ERROR:
      return {
        ...state,
        loadingControlSubmissions: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_CONTROL_LEADERBOARDS_START:
      return {
        ...state,
        loadingControlLeaderboards: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_CONTROL_LEADERBOARDS_SUCCESS:
    case types.LOAD_CONTROL_LEADERBOARDS_ERROR:
      return {
        ...state,
        loadingControlLeaderboards: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_SESSION_START:
      return {
        ...state,
        loadingGameSession: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_SESSION_SUCCESS:
    case types.LOAD_SESSION_ERROR:
      return {
        ...state,
        loadingGameSession: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_CURRENT_QUESTION_START:
      return {
        ...state,
        loadingGameQuestion: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_CURRENT_QUESTION_SUCCESS:
    case types.LOAD_CURRENT_QUESTION_ERROR:
      return {
        ...state,
        loadingGameQuestion: false,
        callsCount: state.callsCount - 1,
      };
    case types.LOAD_LEADERBOARDS_START:
      return {
        ...state,
        loadingGameLeaderboards: true,
        callsCount: state.callsCount + 1,
      };
    case types.LOAD_LEADERBOARDS_SUCCESS:
    case types.LOAD_LEADERBOARDS_ERROR:
      return {
        ...state,
        loadingGameLeaderboards: false,
        callsCount: state.callsCount - 1,
      };
    default:
  }

  if (action.type === types.BEGIN_API_CALL) {
    return { ...state, callsCount: state.callsCount + 1 };
  } else if (
    action.type === types.API_CALL_ERROR ||
    actionTypeEndsInSuccess(action.type)
  ) {
    return { ...state, callsCount: state.callsCount - 1 };
  }

  return state;
};

export default apiStatusReducer;
