const newQuiz = {
  id: null,
  title: "",
  description: "",
  imageUrl: "",
  questions: [],
};

const newQuestion = {
  id: null,
  text: "",
  imageUrl: "",
  revealedImageUrl: "",
  isTimed: false,
  time: null,
  showLeaderboards: false,
  answers: [],
};

const newAnswer = {
  id: null,
  text: "",
  isCorrect: false,
};

const newSession = {
  id: null,
  scheduledTime: "",
  code: "",
  isPrivate: false,
  activity: 0,
  quiz: {},
};

const newParticipant = {
  id: null,
  name: "",
  sessionId: null,
};

module.exports = {
  newQuiz,
  newQuestion,
  newAnswer,
  newSession,
  newParticipant,
};
