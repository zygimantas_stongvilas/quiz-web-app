import {
  API_URL,
  handleResponse,
  handleError,
  getAuthorizationHeader,
} from "./apiUtils";

const baseUrl = `${API_URL}/sessions`;

export const getSessions = () =>
  fetch(
    `${baseUrl}?${new URLSearchParams({
      includePast: false,
      includeCompleted: false,
    })}`
  )
    .then(handleResponse)
    .catch(handleError);

export const loadSession = (sessionCode) =>
  fetch(`${baseUrl}/${sessionCode}`).then(handleResponse).catch(handleError);

export const saveSession = (session) =>
  fetch(`${baseUrl}/${session.id || ""}`, {
    method: session.id ? "PUT" : "POST",
    headers: {
      "content-type": "application/json",
      authorization: getAuthorizationHeader(),
    },
    body: JSON.stringify(session),
  })
    .then(handleResponse)
    .catch(handleError);

export const deleteSession = (sessionId) =>
  fetch(`${baseUrl}/${sessionId}`, {
    method: "DELETE",
    headers: { authorization: getAuthorizationHeader() },
  })
    .then(handleResponse)
    .catch(handleError);

export const joinSession = (sessionId, name) =>
  fetch(`${baseUrl}/${sessionId}/join`, {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify({ name }),
  })
    .then(handleResponse)
    .catch(handleError);

export const loadCurrentQuestion = (sessionId) =>
  fetch(`${baseUrl}/${sessionId}/currentQuestion`)
    .then(handleResponse)
    .catch(handleError);

export const loadCurrentQuestionSubmissions = (sessionId) =>
  fetch(`${baseUrl}/${sessionId}/currentQuestion/submissions`, {
    headers: { authorization: getAuthorizationHeader() },
  })
    .then(handleResponse)
    .catch(handleError);

export const submitAnswer = (sessionId, answer) =>
  fetch(`${baseUrl}/${sessionId}/submitAnswer`, {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(answer),
  })
    .then(handleResponse)
    .catch(handleError);

export const loadLeaderboards = (sessionId) =>
  fetch(`${baseUrl}/${sessionId}/leaderboards`)
    .then(handleResponse)
    .catch(handleError);

export const loadAgenda = (sessionId) =>
  fetch(`${baseUrl}/${sessionId}/agenda`, {
    headers: { authorization: getAuthorizationHeader() },
  })
    .then(handleResponse)
    .catch(handleError);

export const changeActivity = (sessionId) =>
  fetch(`${baseUrl}/${sessionId}/changeActivity`, {
    method: "POST",
    headers: { authorization: getAuthorizationHeader() },
  })
    .then(handleResponse)
    .catch(handleError);

export const changeStatus = (sessionId, status) =>
  fetch(`${baseUrl}/${sessionId}/changeStatus`, {
    method: "POST",
    headers: {
      "content-type": "application/json",
      authorization: getAuthorizationHeader(),
    },
    body: JSON.stringify(status),
  })
    .then(handleResponse)
    .catch(handleError);
