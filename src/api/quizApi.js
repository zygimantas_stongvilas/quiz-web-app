import {
  API_URL,
  handleResponse,
  handleError,
  getAuthorizationHeader,
} from "./apiUtils";

const baseUrl = `${API_URL}/quizzes`;

export const getQuizzes = () =>
  fetch(baseUrl, { headers: { authorization: getAuthorizationHeader() } })
    .then(handleResponse)
    .catch(handleError);

export const getQuiz = (quizId) =>
  fetch(`${baseUrl}/${quizId}`, {
    headers: { authorization: getAuthorizationHeader() },
  })
    .then(handleResponse)
    .catch(handleError);

export const saveQuiz = (quiz) =>
  fetch(`${baseUrl}/${quiz.id || ""}`, {
    method: quiz.id ? "PUT" : "POST",
    headers: {
      "content-type": "application/json",
      authorization: getAuthorizationHeader(),
    },
    body: JSON.stringify(quiz),
  })
    .then(handleResponse)
    .catch(handleError);

export const deleteQuiz = (quizId) =>
  fetch(`${baseUrl}/${quizId}`, {
    method: "DELETE",
    headers: { authorization: getAuthorizationHeader() },
  })
    .then(handleResponse)
    .catch(handleError);

export const getQuizSessions = (quizId) =>
  fetch(`${baseUrl}/${quizId}/sessions`, {
    headers: { authorization: getAuthorizationHeader() },
  })
    .then(handleResponse)
    .catch(handleError);
