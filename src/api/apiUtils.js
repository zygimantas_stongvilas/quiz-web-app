import UnauthorizedError from "./errors/unauthorizedError";

export const API_URL = process.env.REACT_APP_API_URL;

export const handleResponse = async (response) => {
  if (response.ok) {
    if (response.status === 204) {
      return null;
    }
    return response.json();
  }

  if (response.status === 400) {
    const error = await response.text();
    throw new Error(error);
  } else if (response.status === 401) {
    throw new UnauthorizedError();
  }

  console.log(response);
  throw new Error("API response was not ok.");
};

export const handleError = (error) => {
  console.error(`API call failed: ${error}`);
  throw error;
};

export const getAuthorizationHeader = () => {
  return `Bearer ${localStorage.getItem("token")}`;
};
