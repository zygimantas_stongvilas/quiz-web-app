import {
  API_URL,
  handleResponse,
  handleError,
  getAuthorizationHeader,
} from "./apiUtils";

const baseUrl = `${API_URL}/auth`;

export const login = (googleLoginResponse) =>
  fetch(`${baseUrl}/login`, {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify({
      provider: "GOOGLE",
      idToken: googleLoginResponse.tokenId,
    }),
  })
    .then(handleResponse)
    .catch(handleError);

export const logout = () =>
  fetch(`${baseUrl}/logout`).then(handleResponse).catch(handleError);

export const loadUser = () =>
  fetch(`${baseUrl}/me`, {
    headers: {
      authorization: getAuthorizationHeader(),
    },
  })
    .then(handleResponse)
    .catch(handleError);
